package org.somda.reqif.tree

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import org.somda.reqif.ieee.datatype.FormattedString
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.qName
import org.somda.reqif.ieee.rendering.xhtml.Anchor
import org.somda.reqif.ieee.rendering.xhtml.XhtmlConstants
import org.somda.reqif.ieee.spectype.obj.*
import org.w3c.dom.Node
import kotlin.reflect.KClass

class ReqIfTree(
    val rootNodes: List<ReqIfNode>,
    val groups: Multimap<KClass<*>, ReqIfNode>,
    val bookmarks: MutableMap<String, Bookmark>
) {
    companion object {
        fun buildFromDocument(objects: Iterable<ObjectType>): ReqIfTree {
            val expectedDepth = 0
            val iterator = objects.iterator()
            val thisLevelNode = ReqIfNode(VoidType(), null, ArrayList())
            readChildren(thisLevelNode, null, iterator, expectedDepth)

            return ReqIfTree(
                thisLevelNode.children, resolveGroups(thisLevelNode.children),
                resolveBookmarks(thisLevelNode)
            )
        }

        fun findAnnexLetter(node: ReqIfNode): String? {
            var currentNode = node
            while (currentNode.parent != null) {
                val nonNullParent = currentNode.parent!!
                if (nonNullParent.theObject is AnnexType) {
                    return nonNullParent.theObject.value.anticipatedLetter.toString()
                }
                currentNode = nonNullParent
            }
            return null
        }

        private fun resolveBookmarks(node: ReqIfNode): MutableMap<String, Bookmark> {
            val map: MutableMap<String, Bookmark> = HashMap()
            when (val obj = node.theObject) {
                is NormativeReferenceType -> map[obj.value.bookmarkName()] = obj.value.bookmark()
                is BibliographicItemType -> map[obj.value.bookmarkName()] = obj.value.bookmark()
                is RequirementType -> {
                    map[obj.value.bookmarkName()] = obj.value.bookmark()
                    resolveBookmarksInHtml(obj.value.text, map)
                }
                is HeadingType -> if (obj.value.bookmark.isNotEmpty()) {
                    val bookmark = Heading.bookmarkFor(obj.value.bookmark)
                    map[bookmark.name] = bookmark
                }
                is AnnexType -> if (obj.value.bookmark.isNotEmpty()) {
                    val bookmark = Heading.bookmarkFor(obj.value.bookmark)
                    map[bookmark.name] = bookmark
                }
                is TableType -> if (obj.value.bookmark.isNotEmpty()) {
                    val bookmark = Table.bookmarkFor(obj.value.bookmark)
                    val key = when (findAnnexLetter(node)) {
                        null -> bookmark.name
                        else -> "Annex${bookmark.name}"
                    }
                    map[key] = bookmark
                    resolveBookmarksInHtml(obj.value.text, map)
                }
                is PictureType -> if (obj.value.bookmark.isNotEmpty()) {
                    val bookmark = Picture.bookmarkFor(obj.value.bookmark)
                    val key = when (findAnnexLetter(node)) {
                        null -> bookmark.name
                        else -> "Annex${bookmark.name}"
                    }
                    map[key] = bookmark
                }
                is ParagraphType -> {
                    resolveBookmarksInHtml(obj.value.text, map)
                }
                is DefinitionType -> {
                    resolveBookmarksInHtml(obj.value.description, map)
                }
                else -> Unit
            }

            node.children.forEach {
                map.putAll(resolveBookmarks(it))
            }

            return map
        }

        private fun resolveBookmarksInHtml(node: FormattedString, map: MutableMap<String, Bookmark>) {
            if (node.nodeType != Node.ELEMENT_NODE) {
                return
            }
            when (node.qName()) {
                XhtmlConstants.ELEM_A -> node.attributes.getNamedItem("name")?.let {
                    val bm = Anchor.bookmarkFor(it.nodeValue)
                    map[bm.name] = bm
                }
                XhtmlConstants.ELEM_H1,
                XhtmlConstants.ELEM_H2,
                XhtmlConstants.ELEM_H3,
                XhtmlConstants.ELEM_H4,
                XhtmlConstants.ELEM_H5,
                XhtmlConstants.ELEM_H6 -> node.attributes.getNamedItem("id")?.let {
                    val bm = Heading.bookmarkFor(it.nodeValue)
                    map[bm.name] = bm
                }
                else -> {
                    for (i in 0 until node.childNodes.length) {
                        resolveBookmarksInHtml(FormattedString(node.childNodes.item(i)), map)
                    }
                }
            }
        }

        private fun resolveGroups(rootNodes: List<ReqIfNode>): Multimap<KClass<*>, ReqIfNode> {
            val map = ArrayListMultimap.create<KClass<*>, ReqIfNode>()
            for (node in rootNodes) {
                when (node.theObject) {
                    is AnnexType -> add(map, node)
                    is AbstractType -> add(map, node)
                    is BibliographicItemType -> add(map, node)
                    is IntroductionType -> add(map, node)
                    is NormativeReferenceType -> add(map, node)
                    is OverviewType -> add(map, node)
                    is AbbreviationType -> add(map, node)
                    is DefinitionType -> add(map, node)
                    is HeadingType -> add(map, node)
                    is KeywordType -> add(map, node)
                    else -> Unit
                }
                recursiveGrouping(map, node)
            }

            return map
        }

        private fun add(map: Multimap<KClass<*>, ReqIfNode>, reqIfNode: ReqIfNode) {
            map.put(reqIfNode.theObject::class, reqIfNode)
        }

        private fun recursiveGrouping(map: Multimap<KClass<*>, ReqIfNode>, node: ReqIfNode) {
            for (childNode in node.children) {
                when (childNode.theObject) {
                    is AnnexType -> add(map, childNode)
                    is AbstractType -> add(map, childNode)
                    is BibliographicItemType -> add(map, childNode)
                    is IntroductionType -> add(map, childNode)
                    is NormativeReferenceType -> add(map, childNode)
                    is OverviewType -> add(map, childNode)
                    is AbbreviationType -> add(map, childNode)
                    is DefinitionType -> add(map, childNode)
                    is KeywordType -> add(map, childNode)
                    is RequirementType -> add(map, childNode)
                    else -> Unit
                }
                recursiveGrouping(map, childNode)
            }
        }

        private fun readChildren(
            parent: ReqIfNode,
            first: ReqIfNode?,
            iterator: Iterator<ObjectType>,
            expectedDepth: Int
        ): ObjectType? {
            var thisLevelNode = first
            var obj: ObjectType? = null
            loop@ while (iterator.hasNext()) {
                if (obj == null) {
                    obj = iterator.next()
                }
                when {
                    obj.depth == expectedDepth -> {
                        thisLevelNode?.let { parent.children.add(it) }
                        thisLevelNode = ReqIfNode(obj, parent, ArrayList())
                        obj = null
                    }
                    obj.depth > expectedDepth -> {
                        thisLevelNode?.let {
                            obj = readChildren(it, ReqIfNode(obj!!, it, ArrayList()), iterator, expectedDepth + 1)
                        }
                    }
                    else -> {
                        thisLevelNode?.let { parent.children.add(it) }
                        return obj
                    }
                }
            }
            thisLevelNode?.let { parent.children.add(it) }
            return thisLevelNode?.theObject
        }
    }

    fun prettyPrint(): String {
        val builder = StringBuilder()
        prettyPrint(rootNodes, builder, 0)
        return builder.toString()
    }

    private fun prettyPrint(nodes: List<ReqIfNode>, builder: StringBuilder, depth: Int) {
        for (node in nodes) {
            for (i in 0 until depth) {
                builder.append("  ")
            }
            builder.append("- ")
            builder.append(node.theObject)
            builder.append("\n")
            prettyPrint(node.children, builder, depth + 1)
        }
    }

    fun removeDuplicates(nodes: Collection<ReqIfNode>): Collection<ReqIfNode> = nodes
        .filter { it.theObject is NormativeReferenceType || it.theObject is BibliographicItemType }
        .map {
            when (it.theObject) {
                is NormativeReferenceType -> it.theObject.value.name
                is BibliographicItemType -> it.theObject.value.name
                else -> ""
            } to it
        }
        .toMap().values
}