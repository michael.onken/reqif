package org.somda.reqif.tree

import org.somda.reqif.ieee.spectype.obj.ObjectType

data class ReqIfNode(val theObject: ObjectType, val parent: ReqIfNode? = null, val children: MutableList<ReqIfNode> = mutableListOf()) {
    override fun toString() = "ReqIfNode(${theObject})"
}