package org.somda.reqif.ieee.rendering

import org.docx4j.jaxb.Context
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.ObjectFactory
import org.docx4j.wml.P
import org.somda.reqif.ieee.spectype.obj.AbbreviationType
import org.somda.reqif.tree.ReqIfNode

class WordAbbreviationRenderer(
    private val reqIfNode: ReqIfNode
) : WordRenderer {

    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        if (reqIfNode.theObject !is AbbreviationType) {
            System.err.println("Object ${reqIfNode.theObject} is not a definition")
            return contentAccessor
        }

        val paragraph = contentAccessor as P
        paragraph.pPr = createPPr(WordConstants.STYLE_DEFINITIONS)

        val factory: ObjectFactory = Context.getWmlObjectFactory()
        val rPrStyle = factory.createRPr()
        rPrStyle.rStyle = factory.createRStyle()
        rPrStyle.rStyle.`val` = WordConstants.STYLE_DEFINITION_TERM_NUMBERS

        paragraph.content.add(createWordRun(reqIfNode.theObject.value.name + ":", rPrStyle))
        paragraph.content.add(createWordRun(" ${reqIfNode.theObject.value.description}"))
        return contentAccessor
    }
}