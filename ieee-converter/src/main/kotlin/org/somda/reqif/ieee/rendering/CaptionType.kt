package org.somda.reqif.ieee.rendering

enum class CaptionType(val label: String, val regularStyle: String) {
    FIGURE("Figure", WordConstants.STYLE_REGULAR_FIGURE_CAPTION),
    TABLE("Table", WordConstants.STYLE_REGULAR_TABLE_CAPTION)
}