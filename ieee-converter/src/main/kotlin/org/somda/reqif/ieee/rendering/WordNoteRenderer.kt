package org.somda.reqif.ieee.rendering

import org.docx4j.jaxb.Context
import org.docx4j.model.listnumbering.ListNumberingDefinition
import org.docx4j.openpackaging.exceptions.InvalidOperationException
import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.JcEnumeration
import org.docx4j.wml.ObjectFactory
import org.docx4j.wml.RPr
import org.somda.reqif.ieee.spectype.obj.NoteType
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node
import java.math.BigInteger


class WordNoteRenderer(
    private val reqIfNodes: List<ReqIfNode>,
    private var targetPosition: Int = -1,
    private val runRenderer: (ReqIfNode, Node, RPr) -> WordRenderer,
    private val document: WordprocessingMLPackage
) : WordRenderer {

    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        val pPr = createPPr(
            when (reqIfNodes.size) {
                1 -> WordConstants.STYLE_SINGLE_NOTE
                else -> WordConstants.STYLE_SINGLE_NOTE
            },
            JcEnumeration.LEFT
        )

        val factory: ObjectFactory = Context.getWmlObjectFactory()
        for ((index, reqIfNode) in reqIfNodes.withIndex()) {
            val paragraph = contentAccessor.createParagraph()
            paragraph.pPr = pPr

            when (reqIfNodes.size) {
                1 -> paragraph.content.add(createWordRun(WordConstants.LABEL_SINGLE_NOTE))
                else -> paragraph.content.add(
                    createWordRun(
                        WordConstants.LABEL_MULTI_NOTE
                            .format((index + 1).toString())
                    )
                )
            }

            when (targetPosition) {
                -1 -> contentAccessor.content.add(paragraph)
                else -> contentAccessor.content.add(targetPosition + index + 1, paragraph)
            }

            val sizeBefore = contentAccessor.content.size
            runRenderer(reqIfNode, (reqIfNode.theObject as NoteType).value.text, factory.createRPr()).renderTo(paragraph)
            targetPosition += contentAccessor.content.size - sizeBefore
        }

        return contentAccessor
    }

    /* old renderTo - numbering is not implemented yet

    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        val pPr = createPPr(
            when (reqIfNodes.size) {
                1 -> WordConstants.STYLE_SINGLE_NOTE
                else -> WordConstants.STYLE_MULTI_NODE
            }
        )

        val factory: ObjectFactory = Context.getWmlObjectFactory()
        for ((index, reqIfNode) in reqIfNodes.withIndex()) {
            val paragraph = factory.createP()
            paragraph.pPr = pPr

            if (reqIfNodes.size == 1) {
                paragraph.content.add(createWordRun(WordConstants.LABEL_SINGLE_NOTE))
            }

            when (targetPosition) {
                -1 -> contentAccessor.content.add(paragraph)
                else -> contentAccessor.content.add(targetPosition + index + 1, paragraph)
            }

            val sizeBefore = contentAccessor.content.size
            runRenderer((reqIfNode.theObject as NoteType).value.text, factory.createRPr()).renderTo(paragraph)
            targetPosition += contentAccessor.content.size - sizeBefore
        }

        return contentAccessor
    }

     */

    /**
     * For the given list numId, restart the numbering on the specified
     * level at value val.  This is done by creating a new list (ie &lt;w:num&gt;)
     * which uses the existing w:abstractNum.
     * @param numId
     * @param ilvl
     * @param val
     * @return
     */
    fun restart(numId: Long, ilvl: Long, `val`: Long) {

        // Find the abstractNumId

        // (Ensure maps are initialised)
//        if (em == null) {
//            getEmulator()
//        }

        val instanceListDefinitions = document.mainDocumentPart.numberingDefinitionsPart.instanceListDefinitions
        val existingLnd: ListNumberingDefinition = instanceListDefinitions[numId.toString()]
            ?: throw InvalidOperationException("List $numId does not exist")
        val abstractNumIdVal = existingLnd.numNode.abstractNumId.getVal()

        // Generate the new <w:num
        val newNumId = instanceListDefinitions.size + 1
        val factory = Context.getWmlObjectFactory()
        val newNum = factory.createNumberingNum()
        newNum.numId = BigInteger.valueOf(newNumId.toLong())
        val abstractNumId = factory.createNumberingNumAbstractNumId()
        abstractNumId.setVal(abstractNumIdVal)
        newNum.abstractNumId = abstractNumId
        val lvlOverride = factory.createNumberingNumLvlOverride()
        lvlOverride.ilvl = BigInteger.valueOf(ilvl)
        newNum.lvlOverride.add(lvlOverride)
        val start = factory.createNumberingNumLvlOverrideStartOverride()
        start.setVal(BigInteger.valueOf(`val`))
        lvlOverride.startOverride = start

        // Add it to the jaxb object and our hashmap
        //(getJaxbElement() as Numbering).num.add(newNum)
        val listDef =
            ListNumberingDefinition(newNum, document.mainDocumentPart.numberingDefinitionsPart.abstractListDefinitions)
        instanceListDefinitions[listDef.listNumberId] = listDef

        // Return the new numId
        //return newNumId
    }
}