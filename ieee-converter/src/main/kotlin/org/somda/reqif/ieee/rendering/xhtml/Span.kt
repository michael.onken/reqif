package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart
import org.somda.reqif.ieee.ReqIfQNames
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext

class Span(
    private val document: WordprocessingMLPackage,
    private val bookmarks: Map<String, Bookmark>
) : ElementPlugin {
    override fun handledQName() = XhtmlConstants.ELEM_SPAN
    override fun handle(pluginContext: PluginContext) = Div(document, bookmarks).handle(pluginContext)
}