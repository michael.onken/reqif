package org.somda.reqif.ieee.datatype

enum class ObjectTypeName(val reqifName: String) {
    ABBREVIATION("Abbreviation"),
    ABSTRACT("Abstract"),
    ANNEX("Annex"),
    BIBLIOGRAPHIC_ITEM("BibliographicItem"),
    COMPUTER_CODE("ComputerCode"),
    DEFINITION("Definition"),
    HEADING("Heading"),
    INTRODUCTION("Introduction"),
    KEYWORD("Keyword"),
    LIST_ITEM("ListItem"),
    NORMATIVE_REFERENCE("NormativeReference"),
    NOTE("Note"),
    OVERVIEW("Overview"),
    PARAGRAPH("Paragraph"),
    PICTURE("Picture"),
    REQUIREMENT("Requirement"),
    TABLE("Table"),
    ICS_TABLE("IcsTable"),
    VOID("Void");

    companion object {
        private val map = values().associateBy(ObjectTypeName::reqifName)
        fun fromReqIfName(reqIfName: String) = map[reqIfName]
    }
}