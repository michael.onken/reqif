package org.somda.reqif.ieee.rendering

import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.P
import org.docx4j.wml.RPr
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.spectype.obj.Heading
import org.somda.reqif.ieee.spectype.obj.HeadingType
import org.somda.reqif.ieee.spectype.obj.ObjectType
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node

open class WordHeadingRenderer(
    private val reqIfNode: ReqIfNode,
    private val obj: ObjectType,
    private val styleTemplate: String,
    private val runRenderer: (ReqIfNode, Node, RPr) -> WordRenderer,
    private val depthOffset: Int = 0
) : WordRenderer {

    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        val paragraph = contentAccessor as P

        if (obj !is HeadingType) {
            throw Exception("Wrong type passed to WordHeadingRenderer: ${obj::class}")
        }

        paragraph.pPr = createPPr(styleTemplate.format((obj.depth - depthOffset + 1)))
        var cAccess = contentAccessor
        if (obj.value.bookmark.isEmpty()) {
            cAccess = runRenderer(reqIfNode, obj.value.heading, RPr()).renderTo(contentAccessor)
        } else {
            paragraph.appendBookmark(Heading.bookmarkFor(obj.value.bookmark)) {
                cAccess = runRenderer(reqIfNode, obj.value.heading, RPr()).renderTo(it)
            }
        }

        return cAccess
    }
}