package org.somda.reqif.ieee.rendering

import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.RPr
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node
import javax.xml.namespace.QName

class WordRunRenderer(
    private val reqIfNode: ReqIfNode,
    private val node: Node,
    private val style: RPr,
    private val elementPlugins: Map<QName, ElementPlugin>
) : WordRenderer {
    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        return elementPlugins[node.qName()]?.handle(
            PluginContext(
                reqIfNode,
                node,
                contentAccessor,
                style
            ) { reqIfNode: ReqIfNode, node: Node, rPr: RPr -> WordRunRenderer(reqIfNode, node, rPr, elementPlugins) }
        ) ?: contentAccessor
    }
}