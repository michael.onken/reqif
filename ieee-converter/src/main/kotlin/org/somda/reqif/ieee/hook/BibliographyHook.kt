package org.somda.reqif.ieee.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.P
import org.somda.reqif.ieee.rendering.NextParagraph
import org.somda.reqif.ieee.rendering.Renderers
import org.somda.reqif.ieee.spectype.obj.ObjectType
import org.somda.reqif.tree.ReqIfNode

abstract class BibliographyHook(
    private val reqIfNodes: Collection<ReqIfNode>,
    private val renderers: Renderers,
    private val sortByValue: (ObjectType) -> String
) : BookmarkHook {
    override fun callback(bookmark: CTBookmark) {
        val nextParagraph = NextParagraph(bookmark.parent as P)

        reqIfNodes.sortedBy { sortByValue(it.theObject) }.forEach { reqIfNode ->
            nextParagraph.createAndAdd {
                renderers.bibliographicItemRenderer(reqIfNode).renderTo(it)
            }
        }

        nextParagraph.removeOffsetParagraph()
    }
}