package org.somda.reqif.ieee.datatype

import java.lang.Exception

enum class RequirementLevel(val reqIfName: String) {
    NOT_APPLICABLE("n/a"),
    SHALL("SHALL"),
    SHOULD("SHOULD"),
    MAY("MAY");

    companion object {
        private val map = values().associateBy(RequirementLevel::reqIfName)
        fun fromReqIfName(reqIfName: String) = map[reqIfName]
            ?: throw Exception("Enum $reqIfName could not be mapped to ${RequirementLevel::class.java}")
    }
}