package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.wml.BooleanDefaultTrue
import org.docx4j.wml.RPr

class Strong : BasicStylePlugin() {
    override fun handledQName() = XhtmlConstants.ELEM_STRONG
    override fun decorate(rPr: RPr) {
        rPr.b = BooleanDefaultTrue();
    }
}