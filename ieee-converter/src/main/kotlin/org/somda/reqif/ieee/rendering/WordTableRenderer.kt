package org.somda.reqif.ieee.rendering

import org.docx4j.XmlUtils
import org.docx4j.openpackaging.parts.relationships.Namespaces
import org.docx4j.wml.*
import org.somda.reqif.ieee.datatype.PageOrientation
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.spectype.obj.AnnexType
import org.somda.reqif.ieee.spectype.obj.Table
import org.somda.reqif.ieee.spectype.obj.TableType
import org.somda.reqif.tree.ReqIfNode
import org.somda.reqif.tree.ReqIfTree
import org.w3c.dom.Node
import javax.xml.namespace.QName

class WordTableRenderer(
    private val reqIfNode: ReqIfNode,
    private val elementPlugins: Map<QName, ElementPlugin>,
    private val runRenderer: (ReqIfNode, Node, RPr) -> WordRenderer
) : WordRenderer {
    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        val factory = ObjectFactory()
        val parentAccessor = (contentAccessor as P).parent as ContentAccessor

        if (reqIfNode.theObject !is TableType) {
            throw Exception("Expected table type, but found ${reqIfNode.theObject.javaClass}")
        }

        // add landscape information if table is in landscape style
        if (reqIfNode.theObject.value.pageOrientation == PageOrientation.LANDSCAPE) {
            if (contentAccessor.pPr == null) {
                contentAccessor.pPr = factory.createPPr()
            }
            contentAccessor.pPr.sectPr = makePortraitSectPr()
            val newParagraph = makeLandscapeP()
            newParagraph.parent = parentAccessor
            parentAccessor.content.add(parentAccessor.content.indexOf(contentAccessor) + 1, newParagraph)
        }

        var targetAccessor = WordStyledParagraphRenderer(
            reqIfNode,
            reqIfNode.theObject.value.text,
            contentAccessor.pPr ?: reqIfNode.theObject.toWordPPr(),
            elementPlugins,
            runRenderer
        ).renderTo(contentAccessor)

        targetAccessor = WordCaptionRenderer(
            reqIfNode,
            CaptionType.TABLE,
            reqIfNode.theObject.value.caption,
            Table.bookmarkFor(reqIfNode.theObject.value.bookmark),
            ReqIfTree.findAnnexLetter(reqIfNode),
            elementPlugins,
            runRenderer
        ).renderTo(targetAccessor)

        if (reqIfNode.theObject.value.pageOrientation == PageOrientation.PORTRAIT) {
            // portrait orientation does not need extra paragraph; remove in order to avoid blank line
            parentAccessor.content.remove(contentAccessor)
        }

        return targetAccessor
    }

    private fun makeLandscapeP(): P {
        val pAsXml = "<w:p ${Namespaces.W_NAMESPACE_DECLARATION}><w:pPr>" +
                "  <w:pStyle w:val=\"${WordConstants.STYLE_PARAGRAPH}\"/>" +
                "  <w:sectPr>" +
                "    <w:footnotePr>" +
                "      <w:numRestart w:val=\"eachSect\"/>" +
                "    </w:footnotePr>" +
                "    <w:pgSz w:w=\"15840\" w:h=\"12240\" w:orient=\"landscape\" w:code=\"1\"/>" +
                "    <w:pgMar w:top=\"1800\" w:right=\"1440\" w:bottom=\"1800\" w:left=\"1440\" w:header=\"720\" w:footer=\"720\" w:gutter=\"0\"/>" +
                "    <w:lnNumType w:countBy=\"1\"/>" +
                "    <w:cols w:space=\"720\"/>" +
                "    <w:docGrid w:linePitch=\"360\"/>" +
                "  </w:sectPr>" +
                "</w:pPr></w:p>"

        return XmlUtils.unmarshalString(pAsXml) as P
    }

    private fun makePortraitSectPr(): SectPr {
        val sectPrAsXml = "<w:sectPr ${Namespaces.W_NAMESPACE_DECLARATION} xmlns:test='http://test' test:a='test'>" +
                "<w:footnotePr>" +
                "<w:numRestart w:val=\"eachSect\"/>" +
                "</w:footnotePr>" +
                "<w:type w:val=\"continuous\"/>" +
                "<w:pgSz w:w=\"12240\" w:h=\"15840\" w:code=\"1\"/>" +
                "<w:pgMar w:top=\"1440\" w:right=\"1800\" w:bottom=\"1440\" w:left=\"1800\" w:header=\"720\" w:footer=\"720\" w:gutter=\"0\"/>" +
                "<w:lnNumType w:countBy=\"1\"/>" +
                "<w:cols w:space=\"720\"/>" +
                "<w:docGrid w:linePitch=\"360\"/>" +
                "</w:sectPr>"

        return XmlUtils.unmarshalString(sectPrAsXml) as SectPr
    }
}