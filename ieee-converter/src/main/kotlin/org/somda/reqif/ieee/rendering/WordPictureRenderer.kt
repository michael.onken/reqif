package org.somda.reqif.ieee.rendering

import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.RPr
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.spectype.obj.AnnexType
import org.somda.reqif.ieee.spectype.obj.ParagraphType
import org.somda.reqif.ieee.spectype.obj.Picture
import org.somda.reqif.ieee.spectype.obj.PictureType
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node
import javax.xml.namespace.QName

class WordPictureRenderer(
    private val reqIfNode: ReqIfNode,
    private val elementPlugins: Map<QName, ElementPlugin>,
    private val runRenderer: (ReqIfNode, Node, RPr) -> WordRenderer
) : WordRenderer {
    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        val targetAccessor = WordStyledParagraphRenderer(
            reqIfNode,
            reqIfNode.theObject.run {
                when (this) {
                    is ParagraphType -> this.value.text
                    is PictureType -> this.value.text
                    else -> throw Exception("Unsupported picture type detected: $this")
                }
            },
            reqIfNode.theObject.toWordPPr(),
            elementPlugins,
            runRenderer
        ).renderTo(contentAccessor)

        if (reqIfNode.theObject is PictureType) {

            return WordCaptionRenderer(
                reqIfNode,
                CaptionType.FIGURE,
                reqIfNode.theObject.value.caption,
                Picture.bookmarkFor(reqIfNode.theObject.value.bookmark),
                findAnnexLetter(),
                elementPlugins,
                runRenderer
            ).renderTo(targetAccessor)
        }

        return targetAccessor
    }

    private fun findAnnexLetter(): String? {
        var currentNode = reqIfNode
        while (currentNode.parent != null) {
            val nonNullParent = currentNode.parent!!
            if (nonNullParent.theObject is AnnexType) {
                return nonNullParent.theObject.value.anticipatedLetter.toString()
            }
            currentNode = nonNullParent
        }
        return null
    }
}