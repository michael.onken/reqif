package org.somda.reqif.ieee.spectype.obj

import org.somda.reqif.ieee.datatype.FormattedString

data class Abstract(val text: FormattedString)