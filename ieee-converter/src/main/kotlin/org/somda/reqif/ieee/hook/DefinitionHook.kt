package org.somda.reqif.ieee.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.P
import org.somda.reqif.ieee.datatype.ObjectTypeName
import org.somda.reqif.ieee.rendering.NextParagraph
import org.somda.reqif.ieee.rendering.Renderers
import org.somda.reqif.ieee.rendering.WordRenderer
import org.somda.reqif.ieee.spectype.obj.DefinitionType
import org.somda.reqif.tree.ReqIfNode

class DefinitionHook(
    private val reqIfNodes: Collection<ReqIfNode>,
    private val renderers: Renderers
) : BookmarkHook {

    override fun label() = ObjectTypeName.DEFINITION.reqifName

    override fun callback(bookmark: CTBookmark) {
        val nextParagraph = NextParagraph(bookmark.parent as P)
        reqIfNodes.sortedBy { (it.theObject as DefinitionType).value.term }.forEach { reqIfNode ->
            nextParagraph.createAndAdd { renderers.definitionRenderer(reqIfNode).renderTo(it) }
        }
        nextParagraph.removeOffsetParagraph()
    }
}