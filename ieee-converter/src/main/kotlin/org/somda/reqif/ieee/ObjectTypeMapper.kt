package org.somda.reqif.ieee

import org.somda.reqif.ieee.datatype.*
import org.somda.reqif.ieee.rendering.findFirstChild
import org.somda.reqif.ieee.rendering.simpleString
import org.somda.reqif.ieee.spectype.obj.*
import org.w3c.dom.Node

class ObjectTypeMapper(private val xmlAccess: XmlAccess) {
    companion object {
        private var annexLetter = 'A'
    }

    fun toObjectType(depth: Int, node: Node): ObjectType {
        val objectId = xmlAccess.specHierarchyObjectRef(node)
        return when (xmlAccess.resolveObjectTypeName(objectId)) {
            ObjectTypeName.ABBREVIATION -> toAbbreviation(depth, objectId)
            ObjectTypeName.ABSTRACT -> toAbstract(depth, objectId)
            ObjectTypeName.ANNEX -> toAnnex(depth, objectId)
            ObjectTypeName.BIBLIOGRAPHIC_ITEM -> toBibliographicItem(depth, objectId)
            ObjectTypeName.COMPUTER_CODE -> toComputerCode(depth, objectId)
            ObjectTypeName.DEFINITION -> toDefinition(depth, objectId)
            ObjectTypeName.HEADING -> toHeading(depth, objectId)
            ObjectTypeName.INTRODUCTION -> toIntroduction(depth)
            ObjectTypeName.KEYWORD -> toKeyword(depth, objectId)
            ObjectTypeName.LIST_ITEM -> toListItem(depth, objectId)
            ObjectTypeName.NORMATIVE_REFERENCE -> toNormativeReference(depth, objectId)
            ObjectTypeName.NOTE -> toNote(depth, objectId)
            ObjectTypeName.OVERVIEW -> toOverview(depth)
            ObjectTypeName.PARAGRAPH -> toParagraph(depth, objectId)
            ObjectTypeName.PICTURE -> toPicture(depth, objectId)
            ObjectTypeName.REQUIREMENT -> toRequirement(depth, objectId)
            ObjectTypeName.TABLE -> toTable(depth, objectId)
            ObjectTypeName.ICS_TABLE -> toIcsTable(depth, objectId)
            ObjectTypeName.VOID -> toVoid(depth)
        }
    }

    private fun toVoid(depth: Int) =
        VoidType(depth)

    private fun toRequirement(depth: Int, objectId: String) =
        RequirementType(
            depth, Requirement(
                valueFormatted(objectId),
                intValue(objectId, SpecAttribute.REQUIREMENT_NUMBER),
                simpleString(objectId, SpecAttribute.REQUIREMENT_PREFIX),
                RequirementLevel.fromReqIfName(enumValue(objectId, SpecAttribute.REQUIREMENT_LEVEL)),
                SdcRole.fromReqIfName(enumValue(objectId, SpecAttribute.SDC_ROLE)),
                simpleString(objectId, SpecAttribute.ICS_FEATURE),
                simpleString(objectId, SpecAttribute.ICS_GROUP),
                simpleString(objectId, SpecAttribute.ICS_STATUS)
            )
        )

    private fun toParagraph(depth: Int, objectId: String) =
        ParagraphType(depth, Paragraph(valueFormatted(objectId)))

    private fun toPicture(depth: Int, objectId: String) =
        PictureType(
            depth, Picture(
                valueFormatted(objectId),
                bookmark(objectId),
                captionFormatted(objectId)
            )
        )

    private fun toTable(depth: Int, objectId: String) =
        TableType(
            depth, Table(
                valueFormatted(objectId),
                bookmark(objectId),
                captionFormatted(objectId),
                PageOrientation.fromReqIfName(enumValue(objectId, SpecAttribute.PAGE_ORIENTATION))
            )
        )

    private fun toIcsTable(depth: Int, objectId: String) =
        IcsTableType(
            depth, IcsTable(
                valueFormatted(objectId),
                bookmark(objectId),
                captionFormatted(objectId),
                PageOrientation.PORTRAIT
            )
        )

    private fun toOverview(depth: Int) =
        OverviewType(depth)

    private fun toNote(depth: Int, objectId: String) =
        NoteType(depth, Note(valueFormatted(objectId)))

    private fun toNormativeReference(depth: Int, objectId: String) =
        NormativeReferenceType(depth, NormativeReference(keyStr(objectId), valueFormatted(objectId)))

    private fun toListItem(depth: Int, objectId: String) =
        ListItemType(
            depth, ListItem(
                valueFormatted(objectId),
                ListType.fromReqIfName(enumValue(objectId, SpecAttribute.LIST_TYPE))
            )
        )

    private fun toKeyword(depth: Int, objectId: String) =
        KeywordType(depth, Keyword(keyStr(objectId)))

    private fun toIntroduction(depth: Int) =
        IntroductionType(depth)

    private fun toHeading(depth: Int, objectId: String) =
        HeadingType(depth, Heading(keyFormatted(objectId), bookmark(objectId)))

    private fun toDefinition(depth: Int, objectId: String) =
        DefinitionType(depth, Definition(keyStr(objectId), valueFormatted(objectId)))

    private fun toComputerCode(depth: Int, objectId: String) =
        ComputerCodeType(depth, ComputerCode(valueFormatted(objectId)))

    private fun toBibliographicItem(depth: Int, objectId: String) =
        BibliographicItemType(depth, BibliographicItem(keyStr(objectId), valueFormatted(objectId)))

    private fun toAnnex(depth: Int, objectId: String) =
        AnnexType(
            depth, Annex(
                annexLetter++,
                keyFormatted(objectId),
                Normativity.fromReqIfName(enumValue(objectId, SpecAttribute.NORMATIVITY)),
                bookmark(objectId)
            )
        )

    private fun toAbstract(depth: Int, objectId: String) =
        AbstractType(depth, Abstract(valueFormatted(objectId)))

    private fun toAbbreviation(depth: Int, objectId: String) =
        AbbreviationType(depth, Abbreviation(keyStr(objectId), valueStr(objectId)))


    private fun keyStr(objectId: String) =
        xmlAccess.objectAttrOrThrow(
            SpecAttribute.STRING_KEY,
            objectId
        ).attributes.getNamedItem(ReqIfQNames.THE_VALUE.localPart)?.simpleString()
            ?: throw Exception("String value attribute not found for $objectId")

    private fun valueStr(objectId: String) =
        xmlAccess.objectAttrOrThrow(
            SpecAttribute.STRING_VALUE,
            objectId
        ).attributes.getNamedItem(ReqIfQNames.THE_VALUE.localPart)?.simpleString()
            ?: throw Exception("String value attribute not found for $objectId")

    private fun keyFormatted(objectId: String) =
        xmlAccess.objectAttrOrThrow(
            SpecAttribute.FORMATTED_STRING_KEY,
            objectId
        ).findFirstChild(ReqIfQNames.THE_VALUE)?.let { FormattedString(it) }
            ?: throw Exception("Formatted string value attribute not found for $objectId")

    private fun valueFormatted(objectId: String) =
        xmlAccess.objectAttrOrThrow(
            SpecAttribute.FORMATTED_STRING_VALUE,
            objectId
        ).findFirstChild(ReqIfQNames.THE_VALUE)?.let { FormattedString(it) }
            ?: throw Exception("Formatted string value attribute not found for $objectId")

    private fun captionFormatted(objectId: String) =
        xmlAccess.objectAttrOrThrow(
            SpecAttribute.CAPTION,
            objectId
        ).findFirstChild(ReqIfQNames.THE_VALUE)?.let { FormattedString(it) }
            ?: throw Exception("Caption value attribute not found for $objectId")

    private fun enumValue(objectId: String, specAttribute: SpecAttribute) =
        resolveEnumValue(xmlAccess.objectAttrOrThrow(specAttribute, objectId))

    private fun intValue(objectId: String, specAttribute: SpecAttribute) =
        resolveInteger(xmlAccess.objectAttrOrThrow(specAttribute, objectId))

    private fun resolveEnumValue(node: Node): String {
        val ref = node.findFirstChild(ReqIfQNames.VALUES)
            ?.findFirstChild(ReqIfQNames.ENUM_VALUE_REF)
            ?.textContent ?: throw Exception("Enum value reference undefined for node $node")
        return xmlAccess.findNodeById(ref)?.attributes?.getNamedItem("LONG-NAME")?.textContent
            ?: throw Exception("Enum $ref does not have a name")
    }

    private fun resolveInteger(node: Node) =
        node.attributes?.getNamedItem(ReqIfQNames.THE_VALUE.localPart)
            ?.textContent?.toInt() ?: 0

    private fun simpleString(objectId: String, attribute: SpecAttribute) =
        xmlAccess.objectAttr(
            attribute,
            objectId
        )?.attributes?.getNamedItem(ReqIfQNames.THE_VALUE.localPart)?.textContent
            ?: "" // empty string means: no attribute set

    private fun bookmark(objectId: String) = simpleString(objectId, SpecAttribute.BOOKMARK)
}