package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.somda.reqif.ieee.hook.Bookmark

class Heading2(
    document: WordprocessingMLPackage,
    bookmarks: Map<String, Bookmark>
) : HeadingX(document, bookmarks) {
    override fun handledQName() = XhtmlConstants.ELEM_H2
    override fun depth() = 2
}