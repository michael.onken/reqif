package org.somda.reqif.ieee

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.validate
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.validate
import com.github.ajalt.clikt.parameters.types.file
import com.github.ajalt.clikt.parameters.types.int
import org.somda.reqif.ieee.hook.*
import org.somda.reqif.ieee.pocspec.PocSpecXmlExport
import org.somda.reqif.ieee.rendering.Renderers
import org.somda.reqif.ieee.spectype.obj.*
import org.somda.reqif.tree.ReqIfNode
import org.somda.reqif.tree.ReqIfTree
import sun.misc.Unsafe
import java.io.File
import java.io.FileInputStream
import java.lang.reflect.Field

fun main(args: Array<String>) = Main().main(args)

class Main : CliktCommand(name = "ieee-converter") {
    private val outputDirectory by option("--directory", "-d", help = "Output directory")
        .file()
        .validate {
            require(it.exists()) { "'$it' does not exist." }
            require(it.isDirectory) { "'$it' is not a directory." }
        }

    private val overwriteExistingFile by option(
        "--force-overwrite",
        "-f",
        help = "Set to force overwrite DOCM file"
    ).flag(default = false)

    private val plausibilityChecks by option(
        "--plausibility-checks",
        "-p",
        help = "Run plausibility checks on each object"
    ).flag(default = false)

    private val verbose by option("--verbose", "-v", help = "Verbose output").flag(default = false)

    private val icsMinCellHeight by option(
        "--ics-min-cell-height",
        "-h",
        help = "Default height of ICS table cells in twips; default is 1500"
    ).int().default(1500)

    private val icsHideFeature by option(
        "--ics-hide-feature",
        "-r",
        help = "Remove feature column from ICS tables"
    ).flag(default = false)

    private val reqIfFile by argument(name = "FILE", help = "ReqIF input file")
        .file()
        .validate { require(it.exists()) { "'$it' does not exist." } }

    private val pocSpec by option("--pocspecxml", "-x", help = "PoCSpec XML output").flag(default = false)

    override fun run() {
        disableWarning()

        try {
            // Process command line arguments

            // Read ReqIF tree
            val reqIfTree = ReqIfTree.buildFromDocument(ReqIfDocument(FileInputStream(reqIfFile)))
            if (verbose) {
                println(reqIfTree.prettyPrint())
            }
            // Run plausibility checks
            if (plausibilityChecks) {
                PlausibilityChecks(reqIfTree).runPlausibilityChecks()
            }

            if (pocSpec) {
                renderPocSpecXml(reqIfTree)
            } else {
                renderWord(reqIfTree)
            }
        } catch (e: Exception) {
            System.err.println("Script execution stopped: ${e.message}")
            if (verbose) {
                e.printStackTrace()
            }
        }
    }

    private fun disableWarning() {
        try {
            val theUnsafe: Field = Unsafe::class.java.getDeclaredField("theUnsafe")
            theUnsafe.isAccessible = true
            val u: Unsafe = theUnsafe.get(null) as Unsafe
            val cls = Class.forName("jdk.internal.module.IllegalAccessLogger")
            val logger: Field = cls.getDeclaredField("logger")
            u.putObjectVolatile(cls, u.staticFieldOffset(logger), null)
        } catch (e: java.lang.Exception) {
            // ignore
        }
    }

    private fun renderWord(reqIfTree: ReqIfTree) {
        val wordTemplate = WordTemplate()

        // Setup Word renderers
        val renderers = Renderers(
            wordTemplate.wordProcessor,
            reqIfFile.parentFile,
            reqIfTree.bookmarks,
            reqIfTree.groups[RequirementType::class].toList(),
            icsMinCellHeight,
            icsHideFeature
        )

        // Filter items from ReqIF
        val keywords = reqIfTree.groups[KeywordType::class].toList()
        val abstractText = reqIfTree.groups[AbstractType::class].toList()
        val introduction = reqIfTree.groups[IntroductionType::class].toList()
        val overview = reqIfTree.groups[OverviewType::class].toList()
        val mainContentHeadings = reqIfTree.groups[HeadingType::class].toList()
        val annexHeadings = reqIfTree.groups[AnnexType::class].toList()
        val definitions = reqIfTree.groups[DefinitionType::class].toList()
        val abbreviations = reqIfTree.groups[AbbreviationType::class].toList()
        val normativeReferences = reqIfTree.groups[NormativeReferenceType::class].toList()
            .let {
                reqIfTree.removeDuplicates(it)
            }

        val bibliographicItems = (reqIfTree.groups[BibliographicItemType::class].toList())
            .let {
                ArrayList(reqIfTree.removeDuplicates(it))
            }

        // Setup bookmark hook processors
        val bookmarkHooks = arrayOf(
            AbstractHook(abstractText),
            KeywordHook(keywords),
            IntroductionHook(introduction, renderers),
            OverviewHook(overview, renderers),
            NormativeReferenceHook(
                normativeReferences,
                renderers.copy(bibliographicItemRenderer = renderers.normativeReferenceRenderer)
            ),
            DefinitionHook(definitions, renderers),
            AbbreviationHook(abbreviations, renderers),
            MainContentHook(mainContentHeadings, renderers),
            AnnexHook(annexHeadings, renderers.copy(headingRenderer = renderers.annexHeadingRenderer)),
            BibliographicItemHook(bibliographicItems, renderers)
        )

        val wordFilename = reqIfFile.nameWithoutExtension + ".docm"
        val outputFile = File(outputDirectory ?: reqIfFile.parentFile, wordFilename)

        if (outputFile.exists() && !overwriteExistingFile) {
            System.err.println("Output file $outputFile exists already. Use -f option to force overwrite.")
            return
        }

        // Transform word template according to the ReqIF input
        wordTemplate.transform(outputFile) { bookmark ->
            val bookmarkHooksMap = bookmarkHooks.map { it.label() to it }.toMap()
            bookmarkHooksMap[bookmark.name]?.callback(bookmark)
        }
    }

    private fun renderPocSpecXml(reqIfTree: ReqIfTree) {
        PocSpecXmlExport(reqIfFile, outputDirectory, overwriteExistingFile, reqIfTree).writeFile()
    }
}