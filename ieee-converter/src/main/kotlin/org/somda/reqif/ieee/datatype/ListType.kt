package org.somda.reqif.ieee.datatype

enum class ListType(val reqIfName: String) {
    DASHED("Dashed"),
    BULLETED("Bulleted"),
    NUMBERED("Numbered"),
    LATIN("Latin");

    companion object {
        private val map = values().associateBy(ListType::reqIfName)
        fun fromReqIfName(reqIfName: String) = map[reqIfName]
            ?: throw Exception("Enum $reqIfName could not be mapped to ${ListType::class.java}")
    }
}