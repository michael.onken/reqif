package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.XmlUtils
import org.docx4j.jaxb.Context
import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.openpackaging.parts.relationships.Namespaces
import org.docx4j.wml.*
import org.somda.reqif.ieee.rendering.*
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.w3c.dom.Node
import java.math.BigInteger
import javax.xml.bind.JAXBException


class Table(
    private val document: WordprocessingMLPackage
) : ElementPlugin {
    override fun handledQName() = XhtmlConstants.ELEM_TABLE
    override fun handle(pluginContext: PluginContext): ContentAccessor {
        val thisParagraph = pluginContext.contentAccessor as P
        val parentAccessor = thisParagraph.parent as ContentAccessor
        val table = renderTable(document, pluginContext)
        val index = parentAccessor.content.indexOf(thisParagraph)

        parentAccessor.content.add(index + 1, table)
        return pluginContext.contentAccessor
    }

    private fun convertToMatrix(tableNode: Node): List<List<Node>> {
        val result = ArrayList<List<Node>>()
        for (i in 0 until tableNode.childNodes.length) {
            val currentRow = tableNode.childNodes.item(i)
            if (currentRow == null ||
                currentRow.nodeType != Node.ELEMENT_NODE ||
                (currentRow.qName() != XhtmlConstants.ELEM_TABLE_ROW) &&
                (currentRow.qName() != XhtmlConstants.ELEM_TABLE_BODY)
            ) {
                continue
            }

            if (currentRow.qName() == XhtmlConstants.ELEM_TABLE_BODY) {
                return convertToMatrix(currentRow)
            }

            val newMatrixRow = ArrayList<Node>()
            result.add(newMatrixRow)

            for (j in 0 until currentRow.childNodes.length) {
                val currentData = currentRow.childNodes.item(j)
                if (currentData.nodeType == Node.ELEMENT_NODE &&
                    (currentData.qName() == XhtmlConstants.ELEM_TABLE_DATA ||
                            currentData.qName() == XhtmlConstants.ELEM_TABLE_HEADER)
                ) {
                    newMatrixRow.add(currentData)
                }
            }
        }
        return result
    }

    private fun renderTable(document: WordprocessingMLPackage, pluginContext: PluginContext): Tbl {
        val hasHeader = when (pluginContext.source.findFirstChild(XhtmlConstants.ELEM_TABLE_BODY) != null) {
            true -> pluginContext.source.findFirstChild(XhtmlConstants.ELEM_TABLE_BODY)
                ?.findFirstChild(XhtmlConstants.ELEM_TABLE_ROW)
                ?.findFirstChild(XhtmlConstants.ELEM_TABLE_HEADER) != null
            false -> pluginContext.source.findFirstChild(XhtmlConstants.ELEM_TABLE_ROW)
                ?.findFirstChild(XhtmlConstants.ELEM_TABLE_HEADER) != null
        }

        val factory = Context.getWmlObjectFactory()

        // look ahead if we're dealing with a landscape table
        val p = pluginContext.contentAccessor as P
        val pParent = p.parent as ContentAccessor
        val pIndex = pParent.content.indexOf(p)
        val writableWidthTwips = when (pIndex < pParent.content.size - 1 &&
                pParent.content[pIndex + 1] is P &&
                (pParent.content[pIndex + 1] as P).pPr?.sectPr?.pgSz?.orient ?: STPageOrientation.PORTRAIT == STPageOrientation.LANDSCAPE) {
            true -> document.documentModel.sections[0].pageDimensions.writableHeightTwips
            false -> document.documentModel.sections[0].pageDimensions.writableWidthTwips
        }

        val matrix = convertToMatrix(pluginContext.source)
        val cols = when (matrix.isEmpty()) {
            true -> throw Exception("Table has no columns")
            false -> matrix[0].size
        }

        val table = createTable(
            matrix.size,
            cols,
            calculateRowHeights(pluginContext.source),
            calculateColWidths(matrix, cols, writableWidthTwips)
        )
        if (hasHeader) {
            val headerRow = table.content[0] as Tr
            for (i in 0 until cols) {
                val column = headerRow.content[i] as Tc
                column.content.clear()

                val columnParagraph = column.createParagraph()
                columnParagraph.pPr = createPPr(WordConstants.STYLE_TABLE_HEADER)
                pluginContext.runRenderer(pluginContext.reqIfNode, matrix[0][i], RPr()).renderTo(columnParagraph)
            }
        }

        for (i in (if (hasHeader) 1 else 0) until matrix.size) {
            val regularRow = table.content[i] as Tr
            for (j in 0 until cols) {
                val column = regularRow.content[j] as Tc
                column.content.clear()

                val columnParagraph = column.createParagraph()

                columnParagraph.pPr = when (matrix[i][j].attributes.getNamedItem("align")?.textContent ?: "left") {
                    "left" -> createPPr(WordConstants.STYLE_TABLE_LEFT)
                    "center" -> createPPr(WordConstants.STYLE_TABLE_CENTER)
                    "right" -> createPPr(WordConstants.STYLE_TABLE_LEFT)
                        .also { it.jc = Jc() }
                        .also { it.jc.`val` = JcEnumeration.RIGHT }
                    else -> createPPr(WordConstants.STYLE_TABLE_LEFT)
                }

                pluginContext.runRenderer(pluginContext.reqIfNode, matrix[i][j], RPr()).renderTo(columnParagraph)
                column.content.add(columnParagraph)
            }
        }
        addBorders(table, pluginContext.source)
        return table
    }

    private fun calculateColWidths(matrix: List<List<Node>>, cols: Int, writableWidthTwips: Int): List<BigInteger> {
        val colWidths = ArrayList<Double>(cols)
        for (i in 0 until cols) {
            colWidths.add(0.0)
            val individualColWidth = matrix[0][i].attributes.getNamedItem("width")?.textContent ?: ""
            if (individualColWidth.isEmpty()) {
                continue
            }
            if (individualColWidth.takeLast(1) != "%") {
                System.err.println(
                    "Found unsupported table column width field: $individualColWidth. " +
                            "Only percentage is allowed."
                )
                continue
            }
            try {
                colWidths[i] = (individualColWidth.substring(0, individualColWidth.length - 1).toDouble())
            } catch (e: NumberFormatException) {
                System.err.println("Unsupported table column width: $individualColWidth is not a valid value.")
            }
        }

        val numberOfZeroWidths = colWidths.fold(0) { sum, element -> if (element == 0.0) sum + 1 else sum }
        val totalAvailableWidthPercentage = colWidths.fold(0.0) { sum, element -> sum + element }
        val remainingWidthPercentage = 100.0 - totalAvailableWidthPercentage
        val evenDistributedPercentages =
            if (numberOfZeroWidths > 0) remainingWidthPercentage / numberOfZeroWidths else 0.0

        return colWidths.map {
            when (it) {
                0.0 -> BigInteger.valueOf((evenDistributedPercentages / 100.0 * writableWidthTwips).toLong())
                else -> BigInteger.valueOf((it / 100.0 * writableWidthTwips).toLong())
            }
        }
    }

    private fun calculateRowHeights(tableNode: Node): List<BigInteger?> {
        val rowHeights = ArrayList<BigInteger?>()
        for (i in 0 until tableNode.childNodes.length) {
            val currentRow = tableNode.childNodes.item(i)
            if (currentRow == null ||
                currentRow.nodeType != Node.ELEMENT_NODE ||
                (currentRow.qName() != XhtmlConstants.ELEM_TABLE_ROW) &&
                (currentRow.qName() != XhtmlConstants.ELEM_TABLE_BODY)
            ) {
                continue
            }

            if (currentRow.qName() == XhtmlConstants.ELEM_TABLE_BODY) {
                return calculateRowHeights(currentRow)
            }

            val height = currentRow.attributes.getNamedItem("height")?.textContent ?: ""

            val unit = "twips"
            if (height.takeLast(unit.length) == unit) {
                rowHeights.add(BigInteger.valueOf(height.substring(0, height.length - unit.length).toLong()))
            } else {
                rowHeights.add(null)
            }
        }
        return rowHeights
    }

    private fun addBorders(table: Tbl, tableNode: Node) {
        // if no border is given, set 1 as default
        var width = 1L
        try {
            width = tableNode.attributes.getNamedItem("border")?.textContent?.toLong() ?: 1L
        } catch (e: NumberFormatException) {
            // ignore as default is set
        }

        // do not add a border if width is 0
        if (width == 0L) {
            return
        }

        val factory = Context.getWmlObjectFactory()
        table.tblPr = factory.createTblPr()
        val border = factory.createCTBorder()
        border.color = "auto"
        border.sz = BigInteger.valueOf(width * 4L) // 4 appears to be 1/2pt which is Word's default line width
        border.space = BigInteger("0")
        border.setVal(STBorder.SINGLE)
        val borders = factory.createTblBorders()
        borders.bottom = border
        borders.left = border
        borders.right = border
        borders.top = border
        borders.insideH = border
        borders.insideV = border
        table.tblPr.tblBorders = borders

        // Layouting such that table caption sticks to the actual table
        table.tblPr.tblpPr = factory.createCTTblPPr()

        // The numbers that Word generated - seem to be pointless
        // table.tblPr.tblpPr.leftFromText = BigInteger.valueOf(141L)
        // table.tblPr.tblpPr.rightFromText = BigInteger.valueOf(141L)
        table.tblPr.tblpPr.vertAnchor = STVAnchor.TEXT
        table.tblPr.tblpPr.tblpY = BigInteger.ONE

        table.tblPr.tblOverlap = factory.createCTTblOverlap()
        table.tblPr.tblOverlap.`val` = STTblOverlap.NEVER

        table.tblPr.tblW = factory.createTblWidth()
        table.tblPr.tblW.type = "auto"
        table.tblPr.tblW.w = BigInteger.ZERO


        /*
        // Element that Word generated - seems to be unnecessary
        <w:tblPr>
            <w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/>
        </w:tblPr>
         */
    }

    // copy-pasted from TableFactory; changed such that cell width can be determined individually
    private fun createTable(
        rows: Int,
        cols: Int,
        rowHeightTwips: List<BigInteger?>,
        cellWidthTwips: List<BigInteger>
    ): Tbl {
        val tbl = Context.getWmlObjectFactory().createTbl()

        // w:tblPr
        val strTblPr = ("<w:tblPr " + Namespaces.W_NAMESPACE_DECLARATION + ">"
                + "<w:tblStyle w:val=\"TableGrid\"/>"
                + "<w:tblW w:w=\"0\" w:type=\"auto\"/>"
                + "<w:tblLook w:val=\"04A0\"/>"
                + "</w:tblPr>")
        var tblPr: TblPr? = null
        try {
            tblPr = XmlUtils.unmarshalString(strTblPr) as TblPr
        } catch (e: JAXBException) {
            // Shouldn't happen
            e.printStackTrace()
        }
        tbl.tblPr = tblPr

        // <w:tblGrid><w:gridCol w:w="4788"/>
        val tblGrid = Context.getWmlObjectFactory().createTblGrid()
        tbl.tblGrid = tblGrid
        // Add required <w:gridCol w:w="4788"/>
        for (i in 0 until cols) {
            val gridCol = Context.getWmlObjectFactory().createTblGridCol()
            gridCol.w = cellWidthTwips[i]
            tblGrid.gridCol.add(gridCol)
        }

        // Now the rows
        for (j in 0 until rows) {
            val tr = Context.getWmlObjectFactory().createTr()

            rowHeightTwips[j]?.let {
                val ctHeight = CTHeight()
                ctHeight.hRule = STHeightRule.AT_LEAST
                ctHeight.`val` = it
                val trHeight = Context.getWmlObjectFactory().createCTTrPrBaseTrHeight(ctHeight)
                tr.trPr = TrPr()
                tr.trPr.cnfStyleOrDivIdOrGridBefore.add(trHeight)
            }

            tbl.egContentRowContent.add(tr)

            // The cells
            for (i in 0 until cols) {
                val tc = Context.getWmlObjectFactory().createTc()
                tr.egContentCellContent.add(tc)
                val tcPr = Context.getWmlObjectFactory().createTcPr()
                tc.tcPr = tcPr
                // <w:tcW w:w="4788" w:type="dxa"/>
                val cellWidth = Context.getWmlObjectFactory().createTblWidth()
                tcPr.tcW = cellWidth
                cellWidth.type = "dxa"
                cellWidth.w = cellWidthTwips[i]

                // Cell content - an empty <w:p/>
                val p = tc.createParagraph()
                tc.egBlockLevelElts.add(p)
            }
        }
        return tbl
    }
}