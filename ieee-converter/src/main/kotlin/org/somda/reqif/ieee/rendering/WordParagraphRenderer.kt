package org.somda.reqif.ieee.rendering

import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.RPr
import org.somda.reqif.ieee.datatype.defaultFormattedString
import org.somda.reqif.ieee.datatype.simpleFormattedString
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.spectype.obj.*
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node
import javax.xml.namespace.QName

class WordParagraphRenderer(
    private val reqIfNode: ReqIfNode,
    private val elementPlugins: Map<QName, ElementPlugin>,
    private val runRenderer: (ReqIfNode, Node, RPr) -> WordRenderer
) : WordRenderer {
    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        if (WordEnumerationRenderer.isNumberedListItem(reqIfNode.theObject)) {
            return WordEnumerationRenderer(reqIfNode, elementPlugins, runRenderer).renderTo(contentAccessor)
        } else {
            return WordStyledParagraphRenderer(
                reqIfNode,
                reqIfNode.theObject.run {
                    when (this) {
                        is ParagraphType -> this.value.text
                        is ComputerCodeType -> this.value.text
                        is ListItemType -> this.value.text
                        is PlainTextType -> simpleFormattedString(this.value)
                        is DefinitionType -> this.value.description
                        else -> throw Exception("Unsupported paragraph type detected: $this")
                    }
                },
                reqIfNode.theObject.toWordPPr(),
                elementPlugins,
                runRenderer
            ).renderTo(contentAccessor)
        }
    }
}