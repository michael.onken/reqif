package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.wml.BooleanDefaultTrue
import org.docx4j.wml.RPr

class Italic : BasicStylePlugin() {
    override fun handledQName() = XhtmlConstants.ELEM_ITALIC
    override fun decorate(rPr: RPr) {
        rPr.i = BooleanDefaultTrue();
    }
}