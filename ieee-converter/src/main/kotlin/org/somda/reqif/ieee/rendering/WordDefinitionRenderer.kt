package org.somda.reqif.ieee.rendering

import org.docx4j.jaxb.Context
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.ObjectFactory
import org.docx4j.wml.P
import org.docx4j.wml.RPr
import org.somda.reqif.ieee.spectype.obj.DefinitionType
import org.somda.reqif.ieee.spectype.obj.NoteType
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node

class WordDefinitionRenderer(
    private val reqIfNode: ReqIfNode,
    private val paragraphRenderer: (ReqIfNode) -> WordRenderer,
    private val noteRenderer: (List<ReqIfNode>, targetPosition: Int) -> WordRenderer
) : WordRenderer {

    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        if (reqIfNode.theObject !is DefinitionType) {
            System.err.println("Object ${reqIfNode.theObject} is not a definition")
            return contentAccessor
        }

        val notes = reqIfNode.children.filter { it.theObject is NoteType }

        val paragraph = contentAccessor as P
        paragraph.pPr = createPPr(WordConstants.STYLE_DEFINITIONS)

        val factory: ObjectFactory = Context.getWmlObjectFactory()
        val rPrStyle = factory.createRPr()
        rPrStyle.rStyle = factory.createRStyle()
        rPrStyle.rStyle.`val` = WordConstants.STYLE_DEFINITION_TERM_NUMBERS

        paragraph.content.add(createWordRun(reqIfNode.theObject.value.term + ":", rPrStyle))
        paragraph.content.add(createWordRun(" "))
        var cAccess = paragraphRenderer(reqIfNode).renderTo(contentAccessor)

        val parent = contentAccessor.parent as ContentAccessor
        cAccess =
            noteRenderer(notes, parent.content.indexOf(cAccess)).renderTo((cAccess as P).parent as ContentAccessor)
        return cAccess
    }
}