package org.somda.reqif.ieee

import org.somda.reqif.ieee.rendering.xhtml.XhtmlConstants
import javax.xml.namespace.QName

class ReqIfQNames {
    companion object {
        private const val NS_REQIF = "http://www.omg.org/spec/ReqIF/20110401/reqif.xsd"
        private const val PF_REQIF = "rif"

        private const val NS_XHTML = XhtmlConstants.NAMESPACE_XHTML
        private const val PF_XHTML = XhtmlConstants.PREFIX_XHTML

        private const val NS_CFG = "http://eclipse.org/rmf/pror/toolextensions/1.0"
        private const val PF_CFG = "cfg"

        val ATTRIBUTE_DEFINITION_ENUMERATION_REF = QName(
            NS_REQIF, "ATTRIBUTE-DEFINITION-ENUMERATION-REF",
            PF_REQIF
        )
        val ATTRIBUTE_DEFINITION_INTEGER_REF = QName(
            NS_REQIF, "ATTRIBUTE-DEFINITION-INTEGER-REF",
            PF_REQIF
        )
        val ATTRIBUTE_DEFINITION_XHTML_REF = QName(
            NS_REQIF, "ATTRIBUTE-DEFINITION-XHTML-REF",
            PF_REQIF
        )
        val ATTRIBUTE_DEFINITION_STRING_REF = QName(
            NS_REQIF, "ATTRIBUTE-DEFINITION-STRING-REF",
            PF_REQIF
        )
        val ATTRIBUTE_VALUE_ENUMERATION = QName(
            NS_REQIF, "ATTRIBUTE-VALUE-ENUMERATION",
            PF_REQIF
        )
        val ATTRIBUTE_VALUE_INTEGER = QName(
            NS_REQIF, "ATTRIBUTE-VALUE-INTEGER",
            PF_REQIF
        )
        val ATTRIBUTE_VALUE_STRING = QName(
            NS_REQIF, "ATTRIBUTE-VALUE-STRING",
            PF_REQIF
        )
        val ATTRIBUTE_VALUE_XHTML = QName(
            NS_REQIF, "ATTRIBUTE-VALUE-XHTML",
            PF_REQIF
        )
        val CHILDREN = QName(
            NS_REQIF, "CHILDREN",
            PF_REQIF
        )
        val DATATYPES = QName(
            NS_REQIF, "DATATYPES",
            PF_REQIF
        )
        val DEFINITION = QName(
            NS_REQIF, "DEFINITION",
            PF_REQIF
        )
        val ENUM_VALUE_REF = QName(
            NS_REQIF, "ENUM-VALUE-REF",
            PF_REQIF
        )
        val IDENTIFIER = QName(
            NS_REQIF, "IDENTIFIER",
            PF_REQIF
        )
        val LONG_NAME = QName(NS_REQIF, "LONG-NAME")
        val OBJECT = QName(
            NS_REQIF, "OBJECT",
            PF_REQIF
        )
        val SPECIFICATION = QName(
            NS_REQIF, "SPECIFICATION",
            PF_REQIF
        )
        val SPECIFICATIONS = QName(
            NS_REQIF, "SPECIFICATIONS",
            PF_REQIF
        )
        val SPEC_ATTRIBUTES = QName(
            NS_REQIF, "SPEC-ATTRIBUTES",
            PF_REQIF
        )
        val SPEC_HIERARCHY = QName(
            NS_REQIF, "SPEC-HIERARCHY",
            PF_REQIF
        )
        val SPEC_OBJECTS = QName(
            NS_REQIF, "SPEC-OBJECTS",
            PF_REQIF
        )
        val SPEC_OBJECT_REF = QName(
            NS_REQIF, "SPEC-OBJECT-REF",
            PF_REQIF
        )
        val SPEC_OBJECT_TYPE = QName(
            NS_REQIF, "SPEC-OBJECT-TYPE",
            PF_REQIF
        )
        val THE_VALUE = QName(
            NS_REQIF, "THE-VALUE",
            PF_REQIF
        )
        val VALUES = QName(
            NS_REQIF, "VALUES",
            PF_REQIF
        )
        val TYPE = QName(
            NS_REQIF, "TYPE",
            PF_REQIF
        )
        val SPEC_OBJECT_TYPE_REF = QName(
            NS_REQIF, "SPEC-OBJECT-TYPE-REF",
            PF_REQIF
        )
    }

    class NamespaceContext : javax.xml.namespace.NamespaceContext {
        override fun getNamespaceURI(prefix: String?): String {
            return when (prefix) {
                PF_REQIF -> NS_REQIF
                PF_XHTML -> NS_XHTML
                PF_CFG -> NS_CFG
                else -> ""
            }
        }

        override fun getPrefix(namespaceURI: String?): String {
            throw IllegalAccessError("NamespaceContext.getPrefix() required but not implemented!")
        }

        override fun getPrefixes(namespaceURI: String?): MutableIterator<String> {
            throw IllegalAccessError("NamespaceContext.getPrefixes() required but not implemented!")
        }
    }
}