package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPartAbstractImage
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.Drawing
import org.docx4j.wml.ObjectFactory
import org.docx4j.wml.P
import org.somda.reqif.ieee.rendering.Ids
import org.somda.reqif.ieee.rendering.WordConstants
import org.somda.reqif.ieee.rendering.createPPr
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import java.io.File
import java.io.FileInputStream


//<xhtml:object data="testzeug/Antu_face-smile-grin.svg.png" type="image/png">This is what will show if the image can't be displayed.</xhtml:object>

class Object(
    private val document: WordprocessingMLPackage,
    private val inputDir: File
) : ElementPlugin {
    override fun handledQName() = XhtmlConstants.ELEM_OBJECT
    override fun handle(pluginContext: PluginContext): ContentAccessor {
        val path = pluginContext.source.attributes.getNamedItem("data").textContent ?: ""
        val mimeType = pluginContext.source.attributes.getNamedItem("type").textContent.toLowerCase() ?: ""

        if (path.isEmpty()) {
            System.err.println("Found object element without path")
            return pluginContext.contentAccessor
        }
        val file = File(inputDir, path)
        if (!file.exists()) {
            System.err.println("Found object element that points to a non-existing file: ${file.absolutePath}")
            return pluginContext.contentAccessor
        }

        if (mimeType != "image/jpeg" && mimeType != "image/png") {
            System.err.println("Found object element with unsupported mime type: $mimeType")
            return pluginContext.contentAccessor
        }

        val bytes = ByteArray(file.length().toInt())
        FileInputStream(file).use {
            var offset = 0
            var numRead = 0
            while (offset < bytes.size
                && it.read(bytes, offset, bytes.size - offset).also { numRead = it } >= 0
            ) {
                offset += numRead
            }
        }

        val thisParagraph = pluginContext.contentAccessor as P
        val parentAccessor = thisParagraph.parent as ContentAccessor
        insertImageIntoEmptyParagraph(
            document,
            thisParagraph,
            bytes,
            path,
            pluginContext.source.textContent ?: "",
            Ids.next(),
            Ids.next()
        )

        parentAccessor.content.indexOf(thisParagraph)

        return pluginContext.contentAccessor
    }

    private fun insertImageIntoEmptyParagraph(
        wordMLPackage: WordprocessingMLPackage,
        paragraph: P,
        bytes: ByteArray,
        filenameHint: String,
        altText: String,
        id1: Int,
        id2: Int
    ) {
        val imagePart = BinaryPartAbstractImage.createImagePart(wordMLPackage, bytes)
        val inline = imagePart.createImageInline(filenameHint, altText, id1, id2, false)
        val factory = ObjectFactory()
        val run = factory.createR()
        paragraph.pPr = createPPr(WordConstants.STYLE_IMAGE)
        paragraph.content.add(run)
        val drawing = factory.createDrawing()
        run.content.add(drawing)
        drawing.anchorOrInline.add(inline)
    }
}