package org.somda.reqif.ieee.spectype.obj

import org.somda.reqif.ieee.datatype.FormattedString
import org.somda.reqif.ieee.hook.Bookmark

data class BibliographicItem(
    val name: String,
    val description: FormattedString
) {
    companion object {
        const val BOOKMARK_PREFIX = "BibliographicItem:"
    }
    fun bookmarkName() = "$BOOKMARK_PREFIX${Bookmark.sanitizeName(name)}"
    fun bookmark() = Bookmark(bookmarkName(), name)
}