package org.somda.reqif.ieee.rendering.xhtml


import org.docx4j.jaxb.Context
import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.CTBookmark
import org.docx4j.wml.CTMarkupRange
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.ObjectFactory
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.Ids
import org.somda.reqif.ieee.rendering.createWordRun
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.somda.reqif.ieee.rendering.simpleString
import java.net.MalformedURLException
import java.net.URL


class Anchor(
    private val document: WordprocessingMLPackage
) : ElementPlugin {
    companion object {
        private const val BOOKMARK_PREFIX = "Anchor:"
        private const val BOOKMARK_PREFIX_SHORT = "A"

        fun expandShortBookmark(name: String): String? {
            val split = name.split(delimiters = *arrayOf(":"), limit = 2)
            if (split.size == 2 && split[0] == BOOKMARK_PREFIX_SHORT) {
                return "$BOOKMARK_PREFIX${split[1]}"
            }
            return null
        }

        fun bookmarkFor(name: String) =
            Bookmark("$BOOKMARK_PREFIX${Bookmark.sanitizeName(name)}", "$BOOKMARK_PREFIX${Bookmark.sanitizeName(name)}")
    }

    override fun handledQName() = XhtmlConstants.ELEM_A
    override fun handle(pluginContext: PluginContext): ContentAccessor {

        val urlString = pluginContext.source.attributes.getNamedItem("href")?.textContent?.trim() ?: ""
        val anchorName = pluginContext.source.attributes.getNamedItem("name")?.textContent?.trim() ?: ""
        val anchorText = pluginContext.source.simpleString()

        if (urlString.isEmpty() && anchorName.isEmpty()) {
            throw Exception("Anchors need to have either a name or a hyperlink reference (href)")
        }

        if (urlString.isNotEmpty()) {
            try {
                val url = URL(urlString)
                if (!url.toURI().isAbsolute) {
                    throw MalformedURLException()
                }
                println("Process anchor element with href: '$urlString'")
                pluginContext.contentAccessor.content.add(
                    ElementPlugin.createExternalHyperlink(
                        document.mainDocumentPart,
                        urlString,
                        anchorText
                    )
                )
            } catch (e: MalformedURLException) {
                System.err.println("Process malformed URL: '$urlString'")
                appendLinkName(anchorText, pluginContext)
            }
        } else {
            println("Process anchor element with name: '$anchorName'")
            appendBookmark(bookmarkFor(anchorName).name, anchorText, pluginContext)
        }
        return pluginContext.contentAccessor
    }

    private fun appendLinkName(text: String, pluginContext: PluginContext) {
        pluginContext.contentAccessor.content.add(createWordRun(text, pluginContext.parentStyle))
    }

    private fun appendBookmark(anchorName: String, linkName: String, pluginContext: PluginContext) {
        val factory: ObjectFactory = Context.getWmlObjectFactory()
        val id = Ids.nextAsBigInt()

        // Add bookmark start
        val bm: CTBookmark = factory.createCTBookmark()
        bm.id = id
        bm.name = anchorName
        val bmStart = factory.createBodyBookmarkStart(bm)
        pluginContext.contentAccessor.content.add(bmStart)

        appendLinkName(linkName, pluginContext)

        // Add bookmark end
        val mr: CTMarkupRange = factory.createCTMarkupRange()
        mr.id = id
        val bmEnd = factory.createBodyBookmarkEnd(mr)
        pluginContext.contentAccessor.content.add(bmEnd)
    }
}