package org.somda.reqif.ieee.rendering

import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.JcEnumeration
import org.docx4j.wml.P
import org.docx4j.wml.RPr
import org.somda.reqif.ieee.spectype.obj.NormativeReferenceType
import org.somda.reqif.ieee.spectype.obj.NoteType
import org.somda.reqif.tree.ReqIfNode
import org.w3c.dom.Node


class WordNormativeReferenceRenderer(
    private val reqIfNode: ReqIfNode,
    private val noteRenderer: (List<ReqIfNode>, targetPosition: Int) -> WordRenderer,
    private val runRenderer: (ReqIfNode, Node, RPr) -> WordRenderer
) : WordRenderer {
    override fun renderTo(contentAccessor: ContentAccessor): ContentAccessor {
        if (reqIfNode.theObject !is NormativeReferenceType) {
            System.err.println("Object ${reqIfNode.theObject} is not a normative reference")
            return contentAccessor
        }

        val notes = reqIfNode.children.filter { it.theObject is NoteType }

        val paragraph = contentAccessor as P
        paragraph.pPr = createPPr(WordConstants.STYLE_PARAGRAPH, JcEnumeration.LEFT)
        paragraph.content.add(createWordRun("["))
        paragraph.appendBookmark(
            reqIfNode.theObject.value.bookmark(),
            createWordRun(reqIfNode.theObject.value.name)
        )
        paragraph.content.add(createWordRun("]"))

        paragraph.content.add(createWordRun(" "))
        var cAccess = runRenderer(reqIfNode, reqIfNode.theObject.value.description, RPr()).renderTo(paragraph)

        val parent = contentAccessor.parent as ContentAccessor
        cAccess =
            noteRenderer(notes, parent.content.indexOf(paragraph)).renderTo((cAccess as P).parent as ContentAccessor)

        return cAccess
    }
}