package org.somda.reqif.ieee.spectype.obj

data class Keyword(val value: String)