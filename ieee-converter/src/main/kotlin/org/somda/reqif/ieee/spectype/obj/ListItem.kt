package org.somda.reqif.ieee.spectype.obj

import org.somda.reqif.ieee.datatype.FormattedString
import org.somda.reqif.ieee.datatype.ListType

data class ListItem(
    val text: FormattedString,
    val listType: ListType
)