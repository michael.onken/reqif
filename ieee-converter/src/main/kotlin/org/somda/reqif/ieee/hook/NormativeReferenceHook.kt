package org.somda.reqif.ieee.hook

import org.somda.reqif.ieee.datatype.ObjectTypeName
import org.somda.reqif.ieee.rendering.Renderers
import org.somda.reqif.ieee.spectype.obj.NormativeReferenceType
import org.somda.reqif.tree.ReqIfNode

class NormativeReferenceHook(
    reqIfNodes: Collection<ReqIfNode>,
    renderers: Renderers
) : BibliographyHook(reqIfNodes, renderers, { (it as NormativeReferenceType).value.name }) {
    override fun label() = ObjectTypeName.NORMATIVE_REFERENCE.reqifName
}