package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.ContentAccessor
import org.somda.reqif.ieee.ReqIfQNames
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.w3c.dom.Node

class RootElement(
    private val document: WordprocessingMLPackage,
    private val bookmarks: Map<String, Bookmark>
) : ElementPlugin {
    override fun handledQName() = ReqIfQNames.THE_VALUE

    override fun handle(pluginContext: PluginContext): ContentAccessor {
        var cAccess = pluginContext.contentAccessor
        for (i in 0 until pluginContext.source.childNodes.length) {
            val currentNode = pluginContext.source.childNodes.item(i)
            when (currentNode.nodeType) {
                Node.ELEMENT_NODE -> cAccess =
                    pluginContext.runRenderer(pluginContext.reqIfNode, currentNode, pluginContext.parentStyle)
                        .renderTo(cAccess)
                else -> if (currentNode.textContent.trim().isNotEmpty()) {
                    ElementPlugin.appendPlainRun(
                        document.mainDocumentPart, bookmarks, currentNode.textContent,
                        pluginContext.copy(contentAccessor = cAccess)
                    )
                } // ignore leading and trailing white spaces as those are preserved by Word
            }
        }
        return cAccess
    }
}