package org.somda.reqif.ieee.rendering

class WordConstants {
    companion object {
        const val STYLE_TEMPLATE_HEADING_LEVEL = "IEEEStdsLevel%sHeader"
        const val STYLE_TEMPLATE_ANNEX_HEADING_LEVEL = "Heading%s"
        const val STYLE_DEFINITIONS = "IEEEStdsDefinitions"
        const val STYLE_DEFINITION_TERM_NUMBERS = "IEEEStdsDefTermsNumbers"
        const val STYLE_TEMPLATE_NUMBERED_LIST_LEVEL = "IEEEStdsNumberedListLevel%s"
        const val STYLE_UNORDERED_LIST = "IEEEStdsUnorderedList"
        const val STYLE_PARAGRAPH = "IEEEStdsParagraph"
        const val STYLE_COMPUTER_CODE = "IEEEStdsComputerCode"
        const val LABEL_SINGLE_NOTE = "NOTE—"
        const val LABEL_MULTI_NOTE = "NOTE %s—"
        const val STYLE_SINGLE_NOTE = "IEEEStdsSingleNote"
        const val STYLE_MULTI_NODE = "IEEEStdsMultipleNotes"
        const val STYLE_REGULAR_FIGURE_CAPTION = "IEEEStdsRegularFigureCaption"
        const val STYLE_REGULAR_TABLE_CAPTION = "IEEEStdsRegularTableCaption"
        const val STYLE_CAPTION = "Caption"
        const val STYLE_IMAGE = "IEEEStdsImage"
        const val STYLE_TABLE_HEADER = "IEEEStdsTableColumnHead"
        const val STYLE_TABLE_LEFT = "IEEEStdsTableData-Left"
        const val STYLE_TABLE_CENTER = "IEEEStdsTableData-Center"
        const val STYLE_REQUIREMENT = "Requirement"
        const val STYLE_REQUIREMENT_UNORDERED_LIST = "RequirementUnorderedList"
        const val STYLE_TEMPLATE_REQUIREMENT_NUMBERED_LIST_LEVEL = "RequirementUnorderedListLevel%s"
    }
}