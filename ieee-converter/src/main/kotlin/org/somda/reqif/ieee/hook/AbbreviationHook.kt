package org.somda.reqif.ieee.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.P
import org.somda.reqif.ieee.datatype.ObjectTypeName
import org.somda.reqif.ieee.rendering.NextParagraph
import org.somda.reqif.ieee.rendering.Renderers
import org.somda.reqif.ieee.rendering.createPagebreak
import org.somda.reqif.ieee.spectype.obj.AbbreviationType
import org.somda.reqif.tree.ReqIfNode

class AbbreviationHook(
    private val reqIfNodes: Collection<ReqIfNode>,
    private val renderers: Renderers
) : BookmarkHook {
    override fun label() = ObjectTypeName.ABBREVIATION.reqifName

    override fun callback(bookmark: CTBookmark) {
        val nextParagraph = NextParagraph(bookmark.parent as P)

        reqIfNodes.sortedBy { (it.theObject as AbbreviationType).value.name }.forEach { reqIfNode ->
            nextParagraph.createAndAdd {
                renderers.abbreviationRenderer(reqIfNode).renderTo(it)
            }
        }

        //nextParagraph.add { createPagebreak() }
        nextParagraph.removeOffsetParagraph()
    }
}