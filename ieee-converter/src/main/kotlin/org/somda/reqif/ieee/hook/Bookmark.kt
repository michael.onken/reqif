package org.somda.reqif.ieee.hook

data class Bookmark(val name: String, val label: String) {
    companion object {
        fun sanitizeName(name: String) = name.replace("[\\s]+".toRegex(), "_").trim()
    }
}