package org.somda.reqif.ieee.rendering

import com.rits.cloning.Cloner
import org.docx4j.jaxb.Context
import org.docx4j.wml.*
import org.somda.reqif.ieee.ReqIfQNames
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.Foo.bookmarkCounter
import org.somda.reqif.ieee.rendering.xhtml.XhtmlConstants
import org.w3c.dom.Node

private fun appendNodeContent(builder: StringBuilder, node: Node?): StringBuilder {
    var currentNode = node
    while (currentNode != null) {
        if (currentNode.nodeType == Node.TEXT_NODE || currentNode.nodeType == Node.CDATA_SECTION_NODE) {
            builder.append(currentNode.textContent ?: "")
        }

        if (currentNode.childNodes.length > 0) {
            appendNodeContent(builder, currentNode.childNodes.item(0))
        }

        currentNode = currentNode.nextSibling
    }
    return builder
}

fun Node.simpleString() = appendNodeContent(StringBuilder(), this.firstChild)
    .toString()
    .replace("[\\s]+".toRegex(), " ")
    .trim()

fun Node.createWordRun(rPr: RPr? = null): R {
    return createWordRun(textContent, rPr)
}

fun Node.hasAnotherContentElement(): Boolean {
    // table handling is different to paragraph handling in Word
    if (this.qName() == XhtmlConstants.ELEM_TABLE_DATA) {
        return false
    }

    var nextSibling = this.nextSibling
    while (nextSibling != null) {
        when (nextSibling.nodeType) {
            Node.ELEMENT_NODE -> return true
            Node.CDATA_SECTION_NODE, Node.TEXT_NODE -> {
                if (nextSibling.textContent.trim().isNotEmpty()) {
                    return true
                }
            }
        }
        nextSibling = nextSibling.nextSibling
    }

    if (this.parentNode == null) {
        return false
    }
    if (this.parentNode.qName() == ReqIfQNames.THE_VALUE) {
        return false
    }
    return this.parentNode.hasAnotherContentElement()
}

fun RPr.deepCopy(): RPr = Cloner().deepClone(this)

fun createWordRun(content: String, rPr: RPr? = null): R {
    val factory = Context.getWmlObjectFactory()
    val r = factory.createR() ?: throw Exception("Factory created null value")
    rPr?.let { r.rPr = rPr }
    val text = factory.createText()
    text.value = content
    text.space = "preserve"
    r.content.add(text)
    return r
}

fun createLinebreak(): R {
    val factory = Context.getWmlObjectFactory()
    val br = factory.createBr()
    val r = factory.createR();
    r.content.add(br)
    return r
}

fun createPagebreak(): P {
    val factory = Context.getWmlObjectFactory()
    val pageBreak = factory.createBr()
    pageBreak.type = STBrType.PAGE
    val rPageBreak = factory.createR()
    rPageBreak.content.add(pageBreak)
    val pPageBreak = factory.createP()
    pPageBreak.content.add(createWordRun(""))
    pPageBreak.content.add(rPageBreak)
    return pPageBreak
}

fun createPPr(styleId: String, jc: JcEnumeration? = null): PPr {
    val factory = Context.getWmlObjectFactory()
    val pPr = factory.createPPr() ?: throw Exception("Unexpected null pointer")
    val pStyle = factory.createPPrBasePStyle()
    pPr.pStyle = pStyle
    pStyle.setVal(styleId)

    jc?.let {
        pPr.jc = factory.createJc()
        pPr.jc.`val` = jc
    }

    return pPr
}

private object Foo {
    const val bookmarkOffset = 100_000
    var bookmarkCounter = bookmarkOffset - 1
}

fun P.hasContent() = this.content.isNotEmpty()

fun P.appendBookmark(bookmark: Bookmark, renderLabel: (P) -> Unit) {
    val bookmarkId = (bookmarkCounter++).toBigInteger()
    val factory = Context.getWmlObjectFactory()
    val ctBookmarkStart = factory.createCTBookmark()
    ctBookmarkStart.name = bookmark.name
    ctBookmarkStart.id = bookmarkId
    val ctBookmarkEnd = factory.createCTBookmark()
    ctBookmarkEnd.name = bookmark.name
    ctBookmarkEnd.id = bookmarkId
    val cTBookmarkStart = factory.createPBookmarkStart(ctBookmarkStart)
    val cTBookmarkEnd = factory.createPBookmarkEnd(ctBookmarkEnd)

    this.content.add(cTBookmarkStart)
    renderLabel(this)
    this.content.add(cTBookmarkEnd)
}

fun P.appendBookmark(bookmark: Bookmark) = appendBookmark(bookmark, createWordRun(bookmark.label))

fun P.appendBookmark(bookmark: Bookmark, labelRun: R) {
    appendBookmark(bookmark) {
        it.content.add(labelRun)
    }
}

fun ContentAccessor.createParagraph(): P {
    val p = P()
    p.parent = this
    return p
}

fun createFieldCharBeginRun(): R {
    val factory = Context.getWmlObjectFactory()
    val fieldCharBeginRun = factory.createR()
    val fldCharBegin = factory.createFldChar()
    val fldCharBeginWrapped = factory.createRFldChar(fldCharBegin)
    fldCharBegin.fldCharType = STFldCharType.BEGIN
    fieldCharBeginRun.content.add(fldCharBeginWrapped)
    return fieldCharBeginRun
}

fun createFieldCharSeparateRun(): R {
    val factory = Context.getWmlObjectFactory()
    val fieldCharSeparateRun = factory.createR()
    val fldCharSeparate = factory.createFldChar()
    val fldCharSeparateWrapped = factory.createRFldChar(fldCharSeparate)
    fldCharSeparate.fldCharType = STFldCharType.SEPARATE
    fieldCharSeparateRun.content.add(fldCharSeparateWrapped)
    return fieldCharSeparateRun
}

fun createFieldCharEndRun(): R {
    val factory = Context.getWmlObjectFactory()
    val fieldCharEndRun = factory.createR()
    val fldCharEnd = factory.createFldChar()
    val fldCharEndWrapped = factory.createRFldChar(fldCharEnd)
    fldCharEnd.fldCharType = STFldCharType.END
    fieldCharEndRun.content.add(fldCharEndWrapped)
    return fieldCharEndRun
}

fun createInstructionText(instruction: String): R {
    val factory = Context.getWmlObjectFactory()
    val referenceRun = factory.createR()
    val instrText = factory.createText()
    instrText.space = "preserve"
    instrText.value = instruction
    referenceRun.content.add(factory.createRInstrText(instrText))
    return referenceRun
}

fun P.appendNumberedBookmarkReference(bookmark: Bookmark, style: RPr?) {
    content.add(createFieldCharBeginRun())
    content.add(createInstructionText(" REF ${bookmark.name} \\r \\h "))
    content.add(createFieldCharSeparateRun())
    content.add(createWordRun(bookmark.label, style))
    content.add(createFieldCharEndRun())
}

fun P.appendBookmarkReference(bookmark: Bookmark, style: RPr?) {
    content.add(createFieldCharBeginRun())
    content.add(createInstructionText(" REF ${bookmark.name} \\h "))
    content.add(createFieldCharSeparateRun())
    content.add(createWordRun(bookmark.label, style))
    content.add(createFieldCharEndRun())
}

fun createBoolean(value: Boolean = true): BooleanDefaultTrue {
    val boolVal = BooleanDefaultTrue()
    if (!value) {
        boolVal.isVal = false
    }
    return boolVal
}

fun P.appendReferenceToHeading(bookmark: Bookmark, style: RPr?) {
    val factory = Context.getWmlObjectFactory()

    val fieldCharBeginRun = factory.createR()
    val fldCharBegin = factory.createFldChar()
    val fldCharBeginWrapped = factory.createRFldChar(fldCharBegin)
    fldCharBegin.fldCharType = STFldCharType.BEGIN
    fieldCharBeginRun.content.add(fldCharBeginWrapped)
    content.add(fieldCharBeginRun)

    val referenceRun = factory.createR()
    val instrText = factory.createText()
    instrText.space = "preserve"
    instrText.value = " REF ${bookmark.name} \\r \\h "
    referenceRun.content.add(factory.createRInstrText(instrText))
    content.add(referenceRun)

    val fieldCharSeparateRun = factory.createR()
    val fldCharSeparate = factory.createFldChar()
    val fldCharSeparateWrapped = factory.createRFldChar(fldCharSeparate)
    fldCharSeparate.fldCharType = STFldCharType.SEPARATE
    fieldCharSeparateRun.content.add(fldCharSeparateWrapped)
    content.add(fieldCharSeparateRun)

    content.add(createWordRun("[${bookmark.label}]", style))

    val fieldCharEndRun = factory.createR()
    val fldCharEnd = factory.createFldChar()
    val fldCharEndWrapped = factory.createRFldChar(fldCharEnd)
    fldCharEnd.fldCharType = STFldCharType.END
    fieldCharEndRun.content.add(fldCharEndWrapped)
    content.add(fieldCharEndRun)
}