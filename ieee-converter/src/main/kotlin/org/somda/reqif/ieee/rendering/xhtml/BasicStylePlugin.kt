package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.RPr
import org.somda.reqif.ieee.rendering.createWordRun
import org.somda.reqif.ieee.rendering.deepCopy
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.somda.reqif.ieee.rendering.simpleString
import org.w3c.dom.Node

abstract class BasicStylePlugin : ElementPlugin {
    abstract fun decorate(rPr: RPr)

    override fun handle(pluginContext: PluginContext): ContentAccessor {
        val rPr = pluginContext.parentStyle.deepCopy()
        decorate(rPr)
        var cAccess = pluginContext.contentAccessor
        for (i in 0 until pluginContext.source.childNodes.length) {
            val currentNode = pluginContext.source.childNodes.item(i)
            when (currentNode.nodeType) {
                Node.ELEMENT_NODE -> cAccess = pluginContext.runRenderer(pluginContext.reqIfNode, currentNode, rPr).renderTo(cAccess)
                Node.CDATA_SECTION_NODE -> appendBoldRun(currentNode, rPr, pluginContext.contentAccessor)
                Node.TEXT_NODE -> appendBoldRun(currentNode, rPr, pluginContext.contentAccessor)
            }
        }
        return cAccess
    }

    private fun appendBoldRun(node: Node, rPr: RPr, contentAccessor: ContentAccessor) {
        if (node.textContent.isBlank()) {
            return
        }
        contentAccessor.content.add(node.createWordRun(rPr))
    }
}