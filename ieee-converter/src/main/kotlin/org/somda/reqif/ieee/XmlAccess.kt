package org.somda.reqif.ieee

import org.somda.reqif.ieee.datatype.ObjectTypeName
import org.somda.reqif.ieee.rendering.findFirstChild
import org.somda.reqif.ieee.rendering.isA
import org.somda.reqif.ieee.spectype.obj.SpecAttribute
import org.w3c.dom.Document
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import javax.xml.namespace.QName
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory

private typealias QN = ReqIfQNames

class XmlAccess(private val xmlDocument: Document) {
    private val xpFactory = XPathFactory.newInstance()
    private val xPath = {
        val xPath = xpFactory.newXPath()
        xPath.namespaceContext = ReqIfQNames.NamespaceContext()
        xPath
    }

    private val dataTypes = readDataTypes()
    private val specAttributes = readSpecAttributes()
    private val specObjectTypes = readSpecObjectTypes()
    private val specObjects = readSpecObjects()

    fun specifications() = eval<NodeList>(
        "//${elem(QN.SPECIFICATIONS)}/${elem(QN.SPECIFICATION)}",
        XPathConstants.NODESET
    )

    fun resolveObjectTypeName(specObjectId: String): ObjectTypeName {
        val specObjectTypeId =
            specObjects[specObjectId]?.findFirstChild(QN.TYPE)?.findFirstChild(QN.SPEC_OBJECT_TYPE_REF)?.textContent
                ?: throw Exception("Could not resolve object type from object $specObjectId")
        val typeName =
            specObjectTypes[specObjectTypeId]?.attributes?.getNamedItem(QN.LONG_NAME.localPart)?.textContent
                ?: throw Exception("Could not resolve object type name from object type $specObjectTypeId")
        return ObjectTypeName.fromReqIfName(typeName)
            ?: throw Exception("Could not find object type name $typeName")
    }

    fun objectAttrOrThrow(specAttr: SpecAttribute, specObjectId: String): Node {
        return objectAttr(specAttr, specObjectId)
            ?: throw Exception("SpecObject $specObjectId has no attribute $specAttr")
    }

    fun objectAttr(specAttr: SpecAttribute, specObjectId: String): Node? {
        val specObject = specObjects[specObjectId] ?: throw Exception("$specObjectId not found")
        val values = specObject.findFirstChild(ReqIfQNames.VALUES)
            ?: throw Exception("SpecObject $specObjectId has no values")
        for (i in 0 until values.childNodes.length) {
            val objectAttr = values.childNodes.item(i)
            if (!objectAttr.isA(specAttr.attrType.typeName)) {
                continue
            }

            val ref = objectAttr.findFirstChild(ReqIfQNames.DEFINITION)
                ?.findFirstChild(specAttr.attrType.refName)
                ?.textContent
                ?: throw Exception("Reference for $specAttr in SpecObject $specObjectId cannot be resolved")

            val attrName = specAttributes[ref]?.attributes?.getNamedItem(QN.LONG_NAME.localPart)?.textContent
                ?: return null

            if (attrName == specAttr.attrName) {
                return objectAttr
            }
        }

        return null
    }

    private fun readDataTypes(): HashMap<String, Node> {
        val expr = "//${elem(QN.DATATYPES)}/*"
        return readIdentifiedElements(expr)
    }

    private fun readSpecAttributes(): HashMap<String, Node> {
        val expr = "//${elem(QN.SPEC_OBJECT_TYPE)}" +
                "/${elem(QN.SPEC_ATTRIBUTES)}/*"
        return readIdentifiedElements(expr)
    }

    private fun readSpecObjectTypes(): HashMap<String, Node> {
        val expr = "//${elem(QN.SPEC_OBJECT_TYPE)}"
        return readIdentifiedElements(expr)
    }

    private fun readSpecObjects(): HashMap<String, Node> {
        val expr = "//${elem(QN.SPEC_OBJECTS)}/*"
        return readIdentifiedElements(expr)
    }

    private fun readIdentifiedElements(xPathExpr: String): HashMap<String, Node> {
        val elements = HashMap<String, Node>()
        val nodes = xPath().evaluate(xPathExpr, xmlDocument, XPathConstants.NODESET) as NodeList
        for (i in 0 until nodes.length) {
            val specAttributeNode = nodes.item(i)
            val idAttrNode = specAttributeNode.attributes?.getNamedItem(QN.IDENTIFIER.localPart)
                ?: throw Exception("Missing attribute IDENTIFIER in attribute node $specAttributeNode")
            elements[idAttrNode.textContent] = specAttributeNode
        }
        return elements
    }

    fun findNodeById(identifier: String): Node? {
        return eval("//*[@${attr(QN.IDENTIFIER)}='$identifier']", XPathConstants.NODE)
    }

    fun findNodeById(identifier: String, nodes: NodeList): Node? {
        for (i in 0 until nodes.length) {
            val node = nodes.item(i).attributes.getNamedItem(QN.IDENTIFIER.localPart) ?: continue
            if (node.textContent == identifier) {
                return node
            }
        }
        return null
    }

    fun specHierarchyObjectRef(node: Node): String =
        eval("${elem(QN.OBJECT)}/${elem(QN.SPEC_OBJECT_REF)}/text()", node, XPathConstants.STRING)

    private inline fun <reified T> eval(xPathExpression: String, returnType: QName) =
        xPath().evaluate(xPathExpression, xmlDocument, returnType) as T

    private inline fun <reified T> eval(xPathExpression: String, node: Node, returnType: QName) =
        xPath().evaluate(xPathExpression, node, returnType) as T

    private fun elem(qName: QName) =
        (qName.prefix?.let { "$it:" } ?: "") + (qName.localPart ?: "")

    private fun attr(qName: QName) =
        qName.localPart ?: ""
}
