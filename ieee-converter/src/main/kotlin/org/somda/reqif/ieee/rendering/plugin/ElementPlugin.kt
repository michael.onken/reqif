package org.somda.reqif.ieee.rendering.plugin

import org.docx4j.XmlUtils
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart
import org.docx4j.openpackaging.parts.relationships.Namespaces
import org.docx4j.relationships.ObjectFactory
import org.docx4j.relationships.Relationship
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.P
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.appendBookmarkReference
import org.somda.reqif.ieee.rendering.appendNumberedBookmarkReference
import org.somda.reqif.ieee.rendering.appendReferenceToHeading
import org.somda.reqif.ieee.rendering.createWordRun
import org.somda.reqif.ieee.rendering.xhtml.Anchor
import org.somda.reqif.ieee.spectype.obj.*
import java.net.MalformedURLException
import java.net.URL
import javax.xml.namespace.QName

interface ElementPlugin {
    companion object {
        fun appendPlainRun(
            document: MainDocumentPart,
            bookmarks: Map<String, Bookmark>,
            text: String,
            pluginContext: PluginContext
        ) {
            if (text.isBlank()) {
                return
            }

            val str = reduceMultipleWhitespaces(text)
            var openBracket = -1
            var copyOffset = 0
            for (i in str.indices) {
                when (str[i]) {
                    '[' -> openBracket = i
                    ']' -> if (openBracket != -1) {
                        appendPart(str.substring(copyOffset, openBracket), pluginContext)
                        copyOffset = i + 1
                        resolveReference(bookmarks, str.substring(openBracket + 1, i), document, pluginContext)
                    }
                }
            }

            if (copyOffset < str.length) {
                appendPart(str.substring(copyOffset), pluginContext)
            }
        }

        private fun resolveReference(
            bookmarks: Map<String, Bookmark>,
            bookmarkName: String,
            document: MainDocumentPart,
            pluginContext: PluginContext
        ) {
            var bookmark = findHeadingBookmark(bookmarks, bookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendReferenceToHeading(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process heading bookmark: '$bookmarkName'")
                return
            }
            bookmark = findAnchorBookmark(bookmarks, bookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process anchor bookmark: '$bookmarkName'")
                return
            }
            bookmark = findRequirementBookmark(bookmarks, bookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process requirement bookmark: '$bookmarkName'")
                return
            }
            bookmark = findTableBookmark(bookmarks, bookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendNumberedBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process table bookmark: '$bookmarkName'")
                return
            }
            bookmark = findAnnexTableBookmark(bookmarks, bookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process annex table bookmark: '$bookmarkName'")
                return
            }
            bookmark = findPictureBookmark(bookmarks, bookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendNumberedBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process picture bookmark: '$bookmarkName'")
                return
            }
            bookmark = findAnnexPictureBookmark(bookmarks, bookmarkName)
            if (bookmark != null) {
                (pluginContext.contentAccessor as P).appendBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                println("Process picture bookmark: '$bookmarkName'")
                return
            }
            bookmark = findBookmark(bookmarks, bookmarkName)
            if (bookmark != null) {
                appendPart("[", pluginContext)
                (pluginContext.contentAccessor as P).appendBookmarkReference(
                    bookmark,
                    pluginContext.parentStyle
                )
                appendPart("]", pluginContext)
                println("Process bookmark: '$bookmarkName'")
                return
            }
            try {
                val url = URL(bookmarkName)
                if (!url.toURI().isAbsolute) {
                    throw MalformedURLException()
                }
                println("Process hyperlink from brackets: '$bookmarkName'")
                pluginContext.contentAccessor.content.add(
                    createExternalHyperlink(
                        document,
                        bookmarkName,
                        bookmarkName
                    )
                )
            } catch (e: MalformedURLException) {
                System.err.println("Process unknown name in brackets: '$bookmarkName'")
                appendPart(bookmarkName, pluginContext)
            }
        }

        private fun appendPart(text: String, pluginContext: PluginContext) {
            pluginContext.contentAccessor.content.add(createWordRun(text, pluginContext.parentStyle))
        }

        private fun findBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String): Bookmark? {
            return arrayListOf(
                NormativeReference.BOOKMARK_PREFIX,
                BibliographicItem.BOOKMARK_PREFIX
            ).map { "$it${Bookmark.sanitizeName(bookmarkName)}" }.firstOrNull { bookmarks.containsKey(it) }?.let {
                bookmarks[it]
            }
        }

        private fun findAnchorBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Anchor.expandShortBookmark(bookmarkName)?.let { bookmarks[it] }

        private fun findRequirementBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Requirement.expandShortBookmark(bookmarkName)?.let { bookmarks[it] }

        private fun findHeadingBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Heading.expandShortBookmark(bookmarkName)?.let { bookmarks[it] }

        private fun findTableBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Table.expandShortBookmark(bookmarkName)?.let { bookmarks[it] }

        private fun findAnnexTableBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Table.expandShortBookmark(bookmarkName)?.let { bookmarks["Annex$it"] }

        private fun findPictureBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Picture.expandShortBookmark(bookmarkName)?.let { bookmarks[it] }

        private fun findAnnexPictureBookmark(bookmarks: Map<String, Bookmark>, bookmarkName: String) =
            Picture.expandShortBookmark(bookmarkName)?.let { bookmarks["Annex$it"] }


        fun createExternalHyperlink(document: MainDocumentPart, url: String, name: String): P.Hyperlink {
            // We need to add a relationship to word/_rels/document.xml.rels
            // but since its external, we don't use the
            // usual wordMLPackage.getMainDocumentPart().addTargetPart
            // mechanism
            val factory = ObjectFactory()
            val rel: Relationship = factory.createRelationship()
            rel.type = Namespaces.HYPERLINK
            rel.target = url
            rel.targetMode = "External"
            document.relationshipsPart.addRelationship(rel)

            // addRelationship sets the rel's @Id
            val hpl =
                "<w:hyperlink r:id=\"${rel.id}\" " +
                        "xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
                        "xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">" +
                        "<w:r>" +
                        "<w:rPr>" +
                        "</w:rPr>" +
                        "<w:t>$name</w:t>" +
                        "</w:r>" +
                        "</w:hyperlink>"
            return XmlUtils.unmarshalString(hpl) as P.Hyperlink
        }

        fun reduceMultipleWhitespaces(text: String): String =
            text.replace(Regex("\\s+"), " ")

    }

    fun handledQName(): QName
    fun handle(pluginContext: PluginContext): ContentAccessor
}