package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.wml.CTVerticalAlignRun
import org.docx4j.wml.RPr
import org.docx4j.wml.STVerticalAlignRun

class Subscript : BasicStylePlugin() {
    override fun handledQName() = XhtmlConstants.ELEM_SUB
    override fun decorate(rPr: RPr) {
        rPr.vertAlign = CTVerticalAlignRun()
        rPr.vertAlign.`val` = STVerticalAlignRun.SUBSCRIPT
    }
}