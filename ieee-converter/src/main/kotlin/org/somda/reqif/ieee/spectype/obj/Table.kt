package org.somda.reqif.ieee.spectype.obj

import org.somda.reqif.ieee.datatype.FormattedString
import org.somda.reqif.ieee.datatype.PageOrientation
import org.somda.reqif.ieee.hook.Bookmark

data class Table(
    val text: FormattedString,
    val bookmark: String,
    val caption: FormattedString,
    val pageOrientation: PageOrientation
) {
    companion object {
        private const val BOOKMARK_PREFIX = "Table:"
        private const val BOOKMARK_PREFIX_SHORT = "T"

        fun expandShortBookmark(name: String): String? {
            val split = name.split(delimiters = *arrayOf(":"), limit = 2)
            if (split.size == 2 && split[0] == BOOKMARK_PREFIX_SHORT) {
                return "$BOOKMARK_PREFIX${split[1]}"
            }
            return null
        }

        fun bookmarkFor(name: String) =
            Bookmark("$BOOKMARK_PREFIX${Bookmark.sanitizeName(name)}", "$BOOKMARK_PREFIX${Bookmark.sanitizeName(name)}")
    }
}