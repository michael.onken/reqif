package org.somda.reqif.ieee.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.ContentAccessor

interface BookmarkHook {
    fun label(): String
    fun callback(bookmark: CTBookmark)
}