package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.*
import org.somda.reqif.ieee.WordTemplate
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.WordConstants
import org.somda.reqif.ieee.rendering.createPPr
import org.somda.reqif.ieee.rendering.createParagraph
import org.somda.reqif.ieee.rendering.hasAnotherContentElement
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.somda.reqif.ieee.spectype.obj.RequirementType
import org.w3c.dom.Node


open class Div(
    private val document: WordprocessingMLPackage,
    private val bookmarks: Map<String, Bookmark>
) : ElementPlugin {
    override fun handledQName() = XhtmlConstants.ELEM_DIV

    open fun applyStyle(pluginContext: PluginContext) {
        if (pluginContext.source.attributes.getNamedItem("align") != null) {
            val p =  pluginContext.contentAccessor as P
            if (p.pPr == null) {
                p.pPr = PPr()
            }
            p.pPr.jc = Jc()
            p.pPr.jc.`val` = when (pluginContext.source.attributes.getNamedItem("align").textContent ?: "") {
                "left" -> JcEnumeration.LEFT
                "center" -> JcEnumeration.CENTER
                "right" -> JcEnumeration.RIGHT
                else -> JcEnumeration.BOTH
            }
        }
    }

    override fun handle(pluginContext: PluginContext): ContentAccessor {
        applyStyle(pluginContext)

        var cAccess = pluginContext.contentAccessor
        for (i in 0 until pluginContext.source.childNodes.length) {
            val currentNode = pluginContext.source.childNodes.item(i)
            when (currentNode.nodeType) {
                Node.ELEMENT_NODE -> cAccess =
                    pluginContext.runRenderer(pluginContext.reqIfNode, currentNode, pluginContext.parentStyle)
                        .renderTo(cAccess)
                Node.CDATA_SECTION_NODE, Node.TEXT_NODE -> {
                    var text = currentNode.textContent
                    if (i == 0) {
                        text = text.trimStart()
                    }
                    if (i == pluginContext.source.childNodes.length - 1) {
                        text = text.trimEnd()
                    }
                    ElementPlugin.appendPlainRun(
                        document.mainDocumentPart, bookmarks, text,
                        pluginContext.copy(contentAccessor = cAccess)
                    )
                }
            }
        }

        // make forecast whether there is or is no further content
        // if there is content, make new paragraph based on passed one
        // if not, quit without creating a next paragraph
        if (pluginContext.source.hasAnotherContentElement()) {
            val newP = ((pluginContext.contentAccessor as P).parent as ContentAccessor).createParagraph()
            if (pluginContext.reqIfNode.theObject is RequirementType) {
                newP.pPr = createPPr(WordConstants.STYLE_REQUIREMENT)
            }
            val thisP =  pluginContext.contentAccessor.parent as ContentAccessor
            var contentOffset = thisP.content.indexOf(cAccess)
            thisP.content.add(++contentOffset, newP)
            return newP
        }

        return cAccess
    }
}