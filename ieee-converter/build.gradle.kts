import org.gradle.kotlin.dsl.*
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    application
    kotlin("jvm") version "1.3.72"
}

group = "org.somda.reqif"
version = "2.0.1"

repositories {
    mavenCentral()
}

application.mainClassName = "org.somda.reqif.ieee.MainKt"

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    // https://mvnrepository.com/artifact/com.github.ajalt/clikt
    implementation(group = "com.github.ajalt", name = "clikt", version = "2.8.0")

    // https://mvnrepository.com/artifact/javax.xml.bind/jaxb-api
    implementation(group = "javax.xml.bind", name = "jaxb-api", version = "2.3.1")

    // https://mvnrepository.com/artifact/org.slf4j/slf4j-nop
    implementation(group = "org.slf4j", name = "slf4j-nop", version = "1.8.0-beta4")

    // https://mvnrepository.com/artifact/org.eclipse.persistence/org.eclipse.persistence.moxy
    implementation(group = "org.eclipse.persistence", name = "org.eclipse.persistence.moxy", version = "2.7.7")

    // https://mvnrepository.com/artifact/org.docx4j/docx4j-MOXy-JAXBContext
    implementation(group = "org.docx4j", name = "docx4j-JAXB-MOXy", version = "11.1.8")

    // https://mvnrepository.com/artifact/io.github.kostaskougios/cloning
    implementation(group = "io.github.kostaskougios", name = "cloning", version = "1.10.2")

    // https://mvnrepository.com/artifact/com.google.guava/guava
    implementation(group = "com.google.guava", name = "guava", version = "29.0-jre")

    testImplementation("junit", "junit", "4.12")

    compile(kotlin("stdlib-jdk8"))
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

tasks.withType<Jar> {
    // Otherwise you'll get a "No main manifest attribute" error
    manifest {
        attributes["Main-Class"] = "org.somda.reqif.ieee.MainKt"
    }

    // To add all of the dependencies otherwise a "NoClassDefFoundError" error
    from(sourceSets.main.get().output)

    dependsOn(configurations.runtimeClasspath)
    from({
        configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
    })
}